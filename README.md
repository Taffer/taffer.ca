# taffer.ca

[![status-badge](https://ci.codeberg.org/api/badges/13214/status.svg)](https://ci.codeberg.org/repos/13214)

My extremely simple website. Now with my blog and put together via Hugo.

The cool background image was created by
[ronos_art](https://www.fiverr.com/ronos_art) on Fiverr; check out his stuff,
it's great! It's specifically excluded from the CC0 license, I retain all
rights to it.

Deploy with (assumes proper `ssh` configuration and keys):

```sh
$ hugo
...
$ rsync -avz --delete public/ pdx1-shared-a1-35.dreamhost.com:taffer.ca/
...
```

Like what I'm up to? Why not [buy me a coffee?](https://ko-fi.com/taffer)

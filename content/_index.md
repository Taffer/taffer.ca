---
title: "Taffer.ca"
date: 2024-01-18
draft: false
---

[taffer: *noun*, one who taffs] - `chrish at taffer dot ca`

- Mastodon: [@Taffer@mastodon.gamedev.place](https://mastodon.gamedev.place/@Taffer)
- Steam: [punksdad](https://steamcommunity.com/id/punksdad/)
- LinkedIn: [Chris Herborth](https://ca.linkedin.com/in/chrisherborth/)
- Keyoxide: `aspe:keyoxide.org:5QZ2ODE7LGJMI6TRJUECQXJDAY`

Taffer.ca website © 2024 by [Chris Herborth](mailto:chrish@pobox.com) is
licensed under
[Creative Commons BY-NC-ND 4.0](http://creativecommons.org/licenses/by-nc-nd/4.0/)

The cool line art logo was created by
[ronos_art](https://www.fiverr.com/ronos_art) on Fiverr; check out his stuff,
it's great! It's specifically excluded from the CC0 license, I retain all
rights to it.

Like what I'm up to? Why not [buy me a coffee?](https://ko-fi.com/taffer)

[![Codeberg CI Status](https://ci.codeberg.org/api/badges/13214/status.svg)](https://ci.codeberg.org/repos/13214)

---
title: "About"
date: 2024-02-06T16:24:35-05:00
draft: false
showAuthor: false
---

Some information about me, since this is my website.

![Obnoxious Under Construction dot GIF from http://textfiles.com/underconstruction/](HeHeartlandMeadows7432mikeconstruct_site.gif)

My job? Computer [Sisyphus](https://en.wikipedia.org/wiki/Sisyphus) and
[Meme Historian](https://knowyourmeme.com/). I work on cryptography and related
things, at least for now. I like videogames and books.

I currently [work for AWS](https://www.linkedin.com/in/chrisherborth/), but I
definitely don't speak for them.

I run Linux on my personal laptop because it's the least irritating option
*for me*. Your mileage may vary and I'm not going to push you to do whatever
it is that I do. I even
[play video games](https://www.gamingonlinux.com/profiles/11475/) on Linux!

I read a lot, mostly genre stuff, but sometimes other things; you can see
what by following me on Bookwyrm: <https://bookwyrm.social/user/Taffer>

I really don't know what to say; I'm older than you think, I'm married, I've
got a son. I love animals and currently have two naughty dogs (a rat terrier/pug
and a rescue pug).

I program (C, Python, whatever), I dabble in game dev (with Godot), I'm
currently obsessed with Final Fantasy XIV.

I prefer open source but am happy to pay for good things especially games.

![Me, looking thoughtful. My wife took this picture around 2010 in Waterloo, Ontario.](chrish-pondering.jpg "Chris, pondering")

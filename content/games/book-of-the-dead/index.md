---
title: "Untitled Egyptian-Themed Game"
date: 2024-02-29T16:46:55-05:00
draft: false
tags: ["Ancient Egypt", "RPG"]
categories: ["Gamedev"]
showAuthor: false
showHero: true
---

A fantasy role-playing game, set in the ancient Egyptian afterlife.

![Obnoxious Under Construction dot GIF from http://textfiles.com/underconstruction/](HeHeartlandMeadows7432mikeconstruct_site.gif)

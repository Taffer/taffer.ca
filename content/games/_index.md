---
title: "Games"
date: 2024-02-29T16:46:21-05:00
draft: false
tags: ["gamedev", "godot"]
categories: ["Gamedev", "Dev"]
layout: "simple"
---

Ever since I first encountered a computer, I've wanted to make games for them.
I even tried for years to break into the game development business! Luckily(?)
that never happened, so I can just mess with things I'm interested in and like.

The only actual game I've done is the rough-around-the-edges
[Robots Game](https://codeberg.org/Taffer/robots-game) I did for a Godot
Weekly Gamedev Challenge. Sadly, they haven't posted a new challenge since
January 2023…

![Obnoxious Under Construction dot GIF from http://textfiles.com/underconstruction/](HeHeartlandMeadows7432mikeconstruct_site.gif)

## Arcana - The Armageddon Key

A fantasy role-playing game, set in a devastated world on the brink of recovery.

[→ Game Page](arcana)

## Schrödinger's Chicken

A minigame where you make a chicken click a button.

> A chicken and a quantum superposition.

[→ Game Page](schrodingers-chicken)

## Skelly

A small fantasy RPG set in the Skeleton War.

[→ Game Page](skelly)

## Untitled Egyptian-Themed Game

A fantasy role-playing game, set in the ancient Egyptian afterlife.

[→ Game Page](book-of-the-dead)

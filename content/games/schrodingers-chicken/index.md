---
title: "Schrödinger's Chicken"
date: 2024-02-29T16:56:15-05:00
draft: false
tags: ["Schrödinger", "Chicken", "Quantum"]
categories: ["Gamedev"]
showAuthor: false
showHero: true
---

A minigame where you make a chicken click a button.

> A chicken and a quantum superposition.

The superposition is a riff on
[Schrödinger's](https://en.wikipedia.org/wiki/Erwin_Schr%C3%B6dinger) famous
[thought experiment](https://en.wikipedia.org/wiki/Schr%C3%B6dinger's_cat).
The treasure chest contains a coin (yay!) or an explosion (oh no!).

You've got three lives, so make the most of them! :chicken: Can you beat the
high score?

## Update: 2024-04-13

I spent some time working on this again, *finally*. It actually looks like
something now, which is nice.

![A peaceful scene with a chicken and a treasure chest.](featured.png)

I could've sworn I had some notes about how this was going to work, but I
can't seem to find them. :cry:

![A stylized chicken](icon.png)

I was going to have the chance of an explosion change depending on various
things (like how long it's been since the last explosion, maybe something
date-related like Friday the 13th is a bad or good time to play, etc.).
I'll have to see if I can remember anything…

*Really* surprised I didn't write down my ideas for this somewhere.

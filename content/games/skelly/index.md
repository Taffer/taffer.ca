---
title: "Skelly"
date: 2024-02-29T16:46:37-05:00
draft: false
tags: ["RPG", "Skelly", "Skeleton War"]
categories: ["Gamedev"]
showAuthor: false
showHero: true
---

A small fantasy RPG set in the Skeleton War.

![Obnoxious Under Construction dot GIF from http://textfiles.com/underconstruction/](HeHeartlandMeadows7432mikeconstruct_site.gif)

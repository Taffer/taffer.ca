---
title: "Posts"
date: 2024-01-18
draft: false
aliases: ["/home/blog/"]
---

Longer posts; shorter things are on
[Mastodon](https://mastodon.gamedev.place/@Taffer).

---
title: "Wifi Horrors"
date: 2023-04-28
draft: false
tags: ["Linux", "Wifi"]
categories: ["Linux"]
aliases: ["/wifi-horrors/"]
---

In my [last post]({{< ref "/posts/2023/Linux/index.md" >}})'s updates, I
mentioned a persistent wifi problem I've been having off-and-on basically since
installing Linux on this laptop.

The wifi seems to go into a power saving mode or something, even while I'm
using the machine, and while network traffic is happening. Behold the pain
that frequently frustrates me:

![A speedtest.net run showing 0.23 Mbps download and 6 Mbps upload on my gigabit cable connection.](featured.png "A speedtest.net run showing terrible results.")

Shortly before I took that screenshot, I ran the same test on my phone, from
the same spot on my couch. It got over 600 Mbps, so it's definitely the laptop
and not my wifi or my modem. I've seen this laptop hit nearly 700 Mbps doing
the speed test.

My wifi and Bluetooth are provided by an Intel Wi-Fi 6 AX200NGW card. This
always worked fine in Windows, and is great under Linux, except when it slows
down to dial-up levels (ok, maybe ISDN levels). Bluetooth seems solid at all
times.

The facts:

* This is running drivers provided by Intel.
* *Usually* resetting the wifi connection (disconnect, then re-connect) puts
  things back to normal, at least for a while.
* I've followed all of the posts I can find on
  [Ask Ubuntu](https://askubuntu.com/) about this issue, nothing has fixed
  the problem.

I'm close to posting a new question on Ask Ubuntu about this, but I don't
really need someone to paste links to the various posts that have already done
nothing useful for me.

I've also considered replacing the wifi card with a newer 6E model, but I'm
having trouble figuring out if anyone other than Intel makes a compatible
chipset. I don't want to buy an Intel Wi-Fi 6 AX210NGW and go through the
annoyance of performing laptop surgery to just get the same problem again.

Here's a summary of the things I've tried, to no avail:

* added `options iwlwifi power=off` to `/etc/modprobe.d/iwlwifi.conf`
* `sudo iwconfig wlp62s0 power off` (this seems to get turned back on after
  some amount of time, and definitely after I've closed the laptop)
* edited
  `/etc/NetworkManager/conf.d/default-wifi-powersave-on.conf` to change
  `wifi.powersave` from 3 to 2, values suggested by
  [this](https://askubuntu.com/questions/695867/disable-wifi-power-management)
  post on askubuntu.com
* downloaded/installed newer firmware files from Intel's support site, and
  confirmed that the newest version supported by my kernel is being loaded

When the machine is plugged in (nearly all the time), I have all power saving
options disabled other than turning off the screen after some idle time. I
close the laptop when it's not in use and haven't had any trouble with it
waking up in quite some time, so I'm assuming Linux's power management is
working properly.

This post was originally
[here](https://codeberg.org/Taffer/Blog/src/branch/main/2023/04-28-Wifi.md).

## Related Posts

* [Upgrading Kubuntu 22.10 to 23.04]({{< ref "/posts/2023/Linux/index.md" >}})

---
title: "Upgrading Kubuntu 22.10 to 23.04"
date: 2023-04-21
draft: false
tags: ["Linux", "Kubuntu", "Upgrades"]
categories: ["Linux"]
aliases: ["/upgrading-kubuntu-22-10-to-23-04/"]
---

Did an in-place upgrade this morning from Kubuntu 22.10 to 23.04. I was a little
worried about this as the last desktop Linux I used (Mint) suggested doing
a full install from scratch, rather than in-place upgrades. Hopefully they've
fixed that problem, because it's really the *opposite* of being a
noobie-friendly distro. Also, in the distant past, Mandriva (IIRC) used to
pooch its kernel and grub upgrades for me every time.

The update appears to have worked perfectly, with the exception of Telegram
vanishing. That was easy enough to re-install via Flatpak.

With any luck this will fix the "wifi goes into low-power mode randomly" bug
I've been annoyed by since installing 12.10. Luckily(?) the work-around for
that is just to cycle the wifi connection, but it's definitely irritating.

The award-winning MMORPG *Final Fantasy XIV* works, and I haven't noticed
anything else missing or broken.

So far, so good!

This post was originally
[here](https://codeberg.org/Taffer/Blog/src/branch/main/2023/04-21-Linux.md).

## Related Posts

* [Why don't I just switch to Linux?]({{< ref "/posts/2022/Linux/index.md" >}})
* [Why didn't I just switch to Linux *earlier*?]({{< ref "/posts/2022/Linux2/index.md" >}})

## Updates

### 2023-04-22

It's possible the wifi issue has been fixed, but I won't know for sure for a
few days. I was online all of yesterday afternoon without any slowdown.

I did have to re-run the installation script for
[System Tray Extensions](https://github.com/salihmarangoz/system_tray_extensions)
so it could rebuild its Python environment. I need this to turn on my keyboard's
backlight (why the default boot state isn't "on, white" is baffling... good
thing I know how to touch-type).

I also took this opportunity to clean up the last old-style `apt` PPA key I
had (for [Remember The Milk](https://www.rememberthemilk.com/)). The
instructions for doing this can be found on
[askubuntu.com](https://askubuntu.com/questions/1407632/key-is-stored-in-legacy-trusted-gpg-keyring-etc-apt-trusted-gpg).

### 2023-04-22 Again

Literally a minute after I posted the update my wifi speed dropped down to
about 50kbps, soooooo... time to find the instructions for disabling low-power
modes for the Intel Wi-Fi 6 AX200NGW.

Resetting the wifi connection only brings me back up to 130Mbps; normally I get
500-600Mbps. And it's gone back down again while I was typing this.

`sudo modinfo iwlwifi | grep 'parm:'` will show you the current settings. The
suspicious ones (IMHO) are `power_save` and `power_level`:

```
parm:           power_save:enable WiFi power management (default: disable) (bool)
parm:           power_level:default power save level (range from 1 - 5, default: 1) (int)
```

Using `ip link`, I see that my wifi is `wlp62s0`. Turning off power saving on
this interface (via `sudo iw dev wlp62s0 set power_save off`; use
`get power_save` to check the value before/after) does nothing to improve my
speed. Bouncing the connection turns it back on, too. I've added
`options iwlwifi power=off` to `/etc/modprobe.d/iwlwifi.conf`, hopefully
that'll help after a reboot.

Well, that didn't help; `iw dev wlp62s0 get power_save` shows that power saving
is still on.

![An angry cat flipping a table. It says something in Japanese, which I hope is appropriate. I got this from Tumblr via an image search, if you know who made this I'd be happy to provide credit!](featured.gif "Angry cat flips table!")

Maybe `iw` wasn't the right command? `sudo iwconfig wlp62s0 power off` seems to
have gotten me back up to normal wifi speeds at least for now... I still don't
seem to have a way to disable this on boot, and I'm expecting it to reset when
I put the system to sleep.

This is totally riveting isn't it? Hopefully these instructions will help
someone in the future, I dunno.

### 2023-04-22 Again ][

[This](https://askubuntu.com/questions/695867/disable-wifi-power-management)
post on askubuntu.com suggests editing
`/etc/NetworkManager/conf.d/default-wifi-powersave-on.conf` (change
`wifi.powersave` from 3 to 2; magic values!). Guess I'll see...

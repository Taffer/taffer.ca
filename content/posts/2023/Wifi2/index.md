---
title: "Wifi… Optimism?"
date: 2023-05-09
draft: false
tags: ["Linux", "Wifi"]
categories: ["Linux"]
aliases: ["/wifi-optimism/"]
---

As I've mentioned previously ([here]({{< ref "/posts/2023/Linux/index.md" >}})
and [here]({{< ref "/posts/2023/Wifi/index.md" >}})) I'd been having a
persistent wifi problem with the Intel Wi-Fi 6 AX200NGW card in my laptop.

Rather than waste any more time on this *extremely annoying* issue, I spent
$30 CAD on an AX210NGW card and crossed my fingers.

My laptop (an XPG Xenia 15... something AData and Intel co-designed) is a thing
of beauty when performing surgery. A few screws, pop the plastic clips, and the
bottom comes off, revealing easily accessible m.2 slots for two SSDs, two RAM
slots, and the wifi card slot. Swapping network cards took me a couple of
minutes.

It's been a few days and I'm cautiously optimistic. I haven't experienced any
network speed drops since upgrading.

This post was originally
[here](https://codeberg.org/Taffer/Blog/src/branch/main/2023/05-09-Wifi.md).

## Related Posts

* [Upgrading Kubuntu 22.10 to 23.04]({{< ref "/posts/2023/Linux/index.md" >}})
* [Wifi Horrors]({{< ref "/posts/2023/Wifi/index.md" >}})

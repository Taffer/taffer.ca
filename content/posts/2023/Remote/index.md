---
title: "Remote Work"
date: 2023-09-23
draft: false
tags: ["Remote", "WFH"]
categories: ["Work"]
---

I know a lot of people who are looking for work these days, so I've collected a
bunch of links that might help folks. They're mostly oriented towards companies
that allow or encourage remote work and that aren't enormous, since that's my
personal preference.

{{< alert "lightbulb" >}}
As of June 2024 I've stopped updating this post, in favour of the
[Remote Work, 2024 Edition](/posts/2024/remote/)!
{{< /alert >}}

I'd really like to find more green energy companies that aren't just existing
polluters wearing lipstick, but I'm not sure how to find that sort of thing.

**October 13th**: I guess this is evolving into mostly a way to find remote
work? My preference is to never work for a huge company again, but if that's
your jam, who am I to decide for you?

This post was originally
[here](https://codeberg.org/Taffer/Blog/src/branch/main/2023/09-23-Remotes.md).

## B Corp Companies

The B Corp certification is apparently for "beneficial" (thus, the *B*)
companies:

> Certified B Corporations are businesses that meet the highest standards of
> verified social and environmental performance, public transparency, and legal
> accountability to balance profit and purpose.

A list of Canadian certified B Corp companies:
[https://bcorpdirectory.ca/explore/](https://bcorpdirectory.ca/explore/)

A list of American certified B Corp companies:
[https://www.bcorporation.net/en-us/find-a-b-corp/](https://www.bcorporation.net/en-us/find-a-b-corp/)

These seem more inclined to support remote work, and to generally not be
awful.

## "Green" Companies

These three job search sites specialize in "green" companies; I haven't verified
that they're all remote, but I'd expect that to be a strong possibility:

- [MCJ Collective](https://www.mcjcollective.com/)
- [Terra.do](https://terra.do/)
- [Work On Climate](https://workonclimate.org/)

## Remote Companies

First of all, a list of companies "going" remote:
[https://www.levels.fyi/remote/](https://www.levels.fyi/remote/)

Job-finding sites specializing in remote work:

- [AwwCor.Inc](https://awwcor.com/jobboard)
- [crossover.com](https://www.crossover.com/)
- [findasync.com](https://findasync.com/)
- [Funded.club](https://funded.club/startupjobs)
- [HiringCafe](https://hiring.cafe/)
- [nodesk.co](https://nodesk.co/)
- [Remote.co](https://remote.co/)
- [remote.io](https://remote.io/)
- [wellfound.com](https://wellfound.com/remote) - I can't get their profile
  thingy to work (it won't let me pick or enter a location, and won't let me
  progress without one), so I don't know how good this actually is.
- [weworkremotely.com](https://weworkremotely.com/)
- [workingnomads.com](https://www.workingnomads.com/jobs)

Mark Kilby's also got a blog post with some great lists:
[https://www.markkilby.com/remote-jobs/](https://www.markkilby.com/remote-jobs/)

Someone's started a Discord for looking for remote work:
[https://discord.gg/jhZWS4jz7J](https://discord.gg/jhZWS4jz7J)

**NOTE:** Let me know if I've gotten anyone in the wrong buckets here! Also, if
I've gotten any of the Glassdoor links wrong! There sure are a lot of companies
with very similar names…

> Use the Glassdoor links ({{< icon "circle-info" >}}) to get some idea about
> what's wrong with a company; people are much more likely to post reviews when
> they're annoyed or they haven't gotten an offer when they think they should.
> On the other hand, I know at least one FAANG company that asks new employees
> to post a Glassdoor review as part of their first month orientation, before
> they find out what it's really like there. Use your judgement and critical
> thinking!

### Remote-First Companies

These companies are remote-first:

- [8th Light](https://8thlight.com/careers/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-8th-Light-EI_IE792337.11,20.htm)
- [Abnormal Security](https://careers.abnormalsecurity.com/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Abnormal-Security-EI_IE3146005.11,28.htm)
- [Aurora Solar](https://aurorasolar.com/careers/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Aurora-Solar-EI_IE1482878.11,23.htm)
- [Babylist](https://www.babylist.com/jobs)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Babylist-EI_IE1587932.11,19.htm)
- [Beekeeper](https://www.beekeeper.io/careers/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Beekeeper-EI_IE1427524.11,20.htm)
- [Blockchains](https://www.blockchains.com/careers/)
- [Canonical](https://canonical.com/careers)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Canonical-EI_IE230560.11,20.htm)
- [Chainguard](https://www.chainguard.dev/careers)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Chainguard-EI_IE8067937.11,21.htm)
- [CircleCI](https://circleci.com/careers/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-CircleCI-EI_IE919755.11,19.htm)
- [ClearyX](https://clearyx.legal/careers/)
- [Crowdstrike](https://www.crowdstrike.com/careers/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-CrowdStrike-EI_IE795976.11,22.htm)
- [Deel](https://deel.com/careers)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Deel-EI_IE3728271.11,15.htm)
- [Discourse](https://www.discourse.org/jobs)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Discourse-EI_IE1165215.11,20.htm)
- [Docker](https://www.docker.com/careers/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Docker-EI_IE1089506.11,17.htm)
- [Doist](https://doist.com/careers)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Doist-EI_IE1102553.11,16.htm)
- [DuckDuckGo](https://duckduckgo.com/hiring)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-DuckDuckGo-EI_IE1086167.11,21.htm)
- [Elevate Labs](https://jobs.lever.co/elevatelabs)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Elevate-Labs-EI_IE1929250.11,23.htm)
- [Flickr](https://www.flickr.com/jobs)
- [Fly.io](https://fly.io/jobs/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Fly-io-EI_IE7001341.11,17.htm)
- [Function Health](https://function-health.breezy.hr/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Function-Health-EI_IE9638864.11,26.htm)
- [GitBook](https://www.gitbook.com/about#open-roles)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-GitBook-EI_IE1701884.11,18.htm)
- [GitLab](https://about.gitlab.com/jobs/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-GitLab-EI_IE1296544.11,17.htm)
- [Gruntwork](https://www.gruntwork.io/careers/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Gruntwork-io-EI_IE4151546.11,23.htm)
- [hims & hers](https://boards.greenhouse.io/himshers)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-hims-and-hers-EI_IE2090877.11,24.htm)
- [Honeycomb.io](https://www.honeycomb.io/careers)
- [Lumenalta (formerly Clevertech)](https://lumenalta.com/remote-jobs)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Lumenalta-EI_IE781853.11,20.htm)
- [Omnipresent](https://www.omnipresent.com/careers)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Omnipresent-EI_IE289172.11,22.htm)
- [PQShield](https://pqshield.com/careers/)
- [Remote.com](https://remote.com/careers)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Remote-EI_IE3871683.11,17.htm)
- [Shopify](https://www.shopify.com/careers)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Shopify-EI_IE675933.11,18.htm)
- [SmugMug](https://www.smugmug.com/careers)
- [User Interviews](https://boards.greenhouse.io/userinterviews)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-User-Interviews-EI_IE2312809.11,26.htm)
- [Vanta](https://www.vanta.com/company/careers)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Vanta-EI_IE3971334.11,16.htm)

### Remote-Friendly Companies

These companies are (currently) remote friendly, but do have traditional offices:

- [1Password](https://1password.com/careers)
- [A Thinking Ape](https://www.athinkingape.com/careers/#positions)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-A-Thinking-Ape-EI_IE657185.11,25.htm)
- [Airbnb](https://careers.airbnb.com/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Airbnb-EI_IE391850.11,17.htm)
- [Atlassian](https://www.atlassian.com/company/careers)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Atlassian-EI_IE115699.11,20.htm)
- [Auvik](https://www.auvik.com/careers/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Auvik-Networks-EI_IE1371186.11,25.htm)
- [Cash App](https://cash.app/careers)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Cash-App-EI_IE4264347.11,19.htm)
- [Celigo](https://www.celigo.com/careers/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Celigo-EI_IE682785.11,17.htm)
- [Cengage Group](https://www.cengagegroup.com/careers/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Cengage-Group-EI_IE20055.11,24.htm)
- [Census](https://www.getcensus.com/careers)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-CENSUS-EI_IE2853128.11,17.htm)
- [ClickUp](https://clickup.com/careers)
- [Cloudflare](https://www.cloudflare.com/en-ca/careers/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Cloudflare-EI_IE430862.11,21.htm)
- [Copper CRM](https://coppercrm.applytojob.com/apply)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Copper-EI_IE1024075.11,17.htm)
- [Daugherty Business Solutions](https://careers.daugherty.com/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Daugherty-Business-Solutions-EI_IE233317.11,39.htm)
- [DealHub.io](https://dealhub.io/careers/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-DealHub-EI_IE4734967.11,18.htm)
- [Digicert](https://www.digicert.com/careers)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-DigiCert-EI_IE778500.11,19.htm)
- [Discord](https://discord.com/careers)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Discord-EI_IE910317.11,18.htm)
- [DocuSign](https://careers.docusign.com/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-DocuSign-EI_IE307604.11,19.htm)
- [Dropbox](https://jobs.dropbox.com/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Dropbox-EI_IE415350.11,18.htm)
- [Effectual](https://www.effectual.com/careers/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Effectual-EI_IE3542195.11,20.htm)
- [Faire](https://www.faire.com/careers/openings)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Faire-EI_IE1869758.11,16.htm)
- [Grainger](https://jobs.grainger.com/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Grainger-EI_IE711.11,19.htm)
- [Infosec Global](https://www.infosecglobal.com/company/careers)
  (I couldn't find a Glassdoor page for this one, let me know if you find it!)
- [Iterative aka DvC](https://iterative.notion.site/Iterative-ai-is-Hiring-852cb978129645e1906e2c9a878a4d22)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Iterative-EI_IE5154394.11,20.htm)
- [Intrado](https://intrado.jobs.net/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Intrado-EI_IE8054605.11,18.htm)
- [Jenius Bank](https://www.jeniusbank.com/careers)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Jenius-Bank-EI_IE8075096.11,22.htm)
- [Lime](https://www.li.me/about/careers)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Lime-EI_IE1884794.11,15.htm)
- [LogicGate](https://www.logicgate.com/about-us/join-the-team/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-LogicGate-EI_IE1677975.11,20.htm)
- [McGraw Hill](https://careers.mheducation.com/jobs?locations=Remote%2C%2CUnited+States&page=1)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-McGraw-Hill-EI_IE36749.11,22.htm)
- [Metrostar](https://www.metrostar.com/join-us/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-MetroStar-Corporation-EI_IE900326.11,32.htm)
- [MissionWired](https://missionwired.com/about/careers/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-MissionWired-EI_IE992375.11,23.htm)
- [MongoDB](https://www.mongodb.com/company/careers)
- [Mutual of Omaha](https://www.mutualofomaha.com/careers/jobs/search)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Mutual-of-Omaha-EI_IE3678.11,26.htm)
- [Netflix](https://jobs.netflix.com/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Netflix-EI_IE11891.11,18.htm)
- [nVidia](https://www.nvidia.com/en-us/about-nvidia/careers/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-NVIDIA-EI_IE7633.11,17.htm)
- [NextCloud](https://nextcloud.com/jobs/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Nextcloud-EI_IE2885139.11,20.htm)
- [Okta](https://www.okta.com/company/careers/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Okta-EI_IE444756.11,15.htm)
- [Oracle Cloud Infrastructure](https://www.oracle.com/careers/opportunities/oracle-cloud-infrastructure/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Oracle-EI_IE1737.11,17.htm)
- [Patreon](https://www.patreon.com/careers)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Patreon-EI_IE915057.11,18.htm)
- [Proton](https://proton.me/careers)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Proton-privacy-by-default-EI_IE1405328.11,36.htm)
- [Quest Software](https://www.quest.com/company/careers.aspx)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Quest-Software-EI_IE9404.11,25.htm)
- [Rec Room](https://recroom.com/careers#openings)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Rec-Room-EI_IE2450158.11,19.htm)
- [RedHat](https://www.redhat.com/en/jobs/departments)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Red-Hat-EI_IE8868.11,18.htm)
- [Renaissance Learning](https://www.renaissance.com/about-us/careers/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Renaissance-Learning-EI_IE6746.11,31.htm)
- [Scribd](https://www.scribd.com/careers)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Scribd-EI_IE354208.11,17.htm)
- [SiFive](https://www.sifive.com/careers)
- [Signifyd](https://www.signifyd.com/careers/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Signifyd-EI_IE776012.11,19.htm)
- [Slack](https://slack.com/careers)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Slack-EI_IE950758.11,16.htm)
- [Sorcero](https://www.sorcero.com/company/careers)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-sorcero-EI_IE4224698.11,18.htm)
- [Spotify](https://www.lifeatspotify.com/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/spotify)
- [Square/Block](https://careers.squareup.com/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Square-EI_IE6548557.11,17.htm)
- [UPS](https://www.jobs-ups.com/) - I'm still skeptical...
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-UPS-EI_IE3012.11,14.htm)
- [Upwork](https://www.upwork.com/careers/jobs/explore)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Upwork-EI_IE993959.11,17.htm)
- [VGS (Very Good Security](https://www.verygoodsecurity.com/careers)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Very-Good-Security-EI_IE2151677.11,29.htm)
- [Velocity Global](https://velocityglobal.com/company/careers/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Velocity-Global-EI_IE1396093.11,26.htm)
- [WolfSSL](https://www.wolfssl.com/)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-wolfSSL-EI_IE1001795.11,18.htm)
- [Workera](https://workera.ai/careers)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Workera-EI_IE6081733.11,18.htm)
- [Workiva](https://www.workiva.com/careers)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Workiva-EI_IE833668.11,18.htm)
- [Yubico](https://jobs.lever.co/yubico)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Yubico-EI_IE1051192.11,17.htm)

### Unknown

Remote or remote-friendly companies collected from my network, I couldn't tell
where these belonged:

- [Upstart](https://www.upstart.com/careers/open-roles); I literally can't
  access their site due to Cloudflare being broken.
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-Upstart-EI_IE962297.11,18.htm)
- [VAST Data](https://vastdata.com/careers)
  [{{< icon "circle-info" >}}](https://www.glassdoor.ca/Overview/Working-at-VAST-Data-EI_IE3166796.11,20.htm)

## More

There are *lots* of companies of all sizes doing remote work who are giddy with
excitement watching the FAANGs ("MANGA" I suppose, but I refuse to go along
with Facebook's dumb rebranding) bend over for commercial real estate
interests. If you know of any, let me know, either here as an
[Issue](https://codeberg.org/Taffer/Blog/issues), via
[email](mailto:chrish@pobox.com), or on
[Mastodon](https://mastodon.gamedev.place/@Taffer).

~~I'll keep updating this as long as I keep getting new information!~~

**Further updates will happen in the [2024 Edition](/posts/2024/remote/)!**

## Updates

### 2024-06

I'm going to clean this up and make a "2024 edition", but I wanted to add
these before I get around to it:

- 2024-06-18 - Added 1Password, Blockchains, ClearyX, Flickr, MongoDB, PQShield,
  SiFive, Smugmug; added Remote.co job site (first one in ages!)

### 2024-05

- 2024-05-16 - Added DigiCert

### 2024-04

- 2024-04-26 - Added Doist, Elevate Labs, GitBook
- 2024-04-11 - Added Proton
- 2024-04-04 - Added Chainguard and Vanta
- 2024-04-02 - Added Function Health

### 2024-03

- 2024-03-13 - Added Glassdoor links where I could.
- 2024-03-01 - Added Discourse

### 2024-02

- 2024-02-24 - Added Fly.io and reformatted the Update list.
- 2024-02-15 - Added CircleCI, Mutual of Omaha
- 2024-02-02 - Added Iterative.ai (aka DvC.ai)

### 2024-01

- 2024-01-31 - Added Yubico
- 2024-01-27 - Added the section about B Corp companies.
- 2024-01-16 - Started trying to split up the companies into Remote First and
  Remote Friendly buckets.
- 2024-01-11 - Added two new job boards and a pile of places hiring remotely,
  courtesy of my friend Tom K.

### 2023-12

- 2023-12-23 - Added NextCloud

### 2023-11

- 2023-11-11 - Glommed updates together by month. Added RedHat, Shopify.
- 2023-11-04 - Added Green Jobs section

### 2023-10

- 2023-10-22 - Added A Thinking Ape and Gruntwork
- 2023-10-18 - Added crossover.com, remote work Discord
- 2023-10-13 - Added a bunch of remote places from LinkedIn, and a **TODO** for
  organizing the list better now that it's growing.
- 2023-10-07 - Added Scribd.  Added a link to Mark Kilby's similar blog
  post about remote jobs. Added Auvik. Added links to their careers pages
  (except WolfSSL, they don't have a dedicated page).

### 2023-09

- 2023-09-30 - Added Infosec Global. Reverse-sorted the Updates; I'm hoping to
  get updates to this post for a long time.
- 2023-09-25 - Added GitLab and Atlassian to remote companies.
- 2023-09-23 - [HiringCafe](https://hiring.cafe/) is some sort of ML-scraped
  job list tracking some huge number of companies, looks pretty easy to search
  via keywords. Their "Espresso" and "Mochi" plans look interesting (or "Boba"
  for more junior people or career switchers). I also added some notes above to
  [wellfound.com](https://wellfound.com/remote).
  [No Whiteboard.org](https://www.nowhiteboard.org/) is interesting, it's a
  job site for companies who don't do whiteboarding "puzzle" interviews
  (which is a good idea, because those don't reflect your work skills at all).

## Related Posts

- None, this is my first post about jobs...

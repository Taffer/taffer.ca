---
title: "Why don't I just switch to Linux?"
date: 2022-10-10
draft: false
tags: ["Linux"]
categories: ["Linux"]
aliases: ["/why-dont-i-just-switch-to-linux/"]
---

## Why don't I just switch to Linux?

I *like* Linux. I feel more comfortable in Linux. So why am I using Windows 11
on my laptop?

This post was originally
[here](https://codeberg.org/Taffer/Blog/src/branch/main/2022/10-10-Linux.md).

(This was originally published on GitHub as a Gist; I've mutated it here as a
proper blog post.)

### Must-have software

Most of the apps I use day-to-day are portable or have Linux equivalents that
are pretty decent. There are a few things that I "need" that don't:

* Backblaze backup client - Yeah, I know I can access B2 easily from Linux, but
  I haven't found a fire-and-forget backup client that works as well as
  Backblaze on Windows or macOS.
* Affinity Photo and Affinity Designer - Granted, I don't use these much, and I
  could probably get by with Krita and Inkscape, but I *just* bought these.
  Sunk cost fallacy!
* The critically acclaimed MMORPG Final Fantasy XIV - XIVLauncher supports
  running the Windows version on Linux, but I've had a lot of trouble with
  keyboard support. I swap CapsLock and Ctrl on all my systems, but
  XIVLauncher's Wine seems to treat it as a CtrlLock, which is not useful. I
  don't remember having this problem with City of Heroes.

Sync.com would've been on this list util very recently; Filen.io seems to fit
that niche, although I'm still testing it.

All of these things work well under macOS, and those Apple Silicon devices are
*nice* (I use one for work). It'll be 2-3 years before I upgrade my laptop
though...

## Updates

### 2022-10-11

Potential backup software (targeting B2, using
[`restic`](https://github.com/restic/restic)):

* Relica - https://relicabackup.com/

### 2022-10-24

Solved issue:

* The critically acclaimed MMORPG Final Fantasy XIV - XIVLauncher supports
  running the Windows version on Linux, but I've had a lot of trouble with
  keyboard support. I swap CapsLock and Ctrl on all my systems, but
  XIVLauncher's Wine seems to treat it as a CtrlLock, which is not useful. I
  don't remember having this problem with City of Heroes.

Root cause and solution:

* Under X, Wine uses a *very* low-level API for getting keyboard events. In
  Mint and KDE, they keyboard configuration I used to map CapsLock->Ctrl
  happens at a higher level... after Wine already got the event.
* Fix is to remap using a lower-level tool: https://github.com/samvel1024/kbct

### 2022-11-01

I was going to try [Duplicati](https://www.duplicati.com/) for backups, but it
doesn't work on systems that use OpenSSL 3.x, like Kubuntu 22.10. So I guess
I'll write some shell scripts and use `restic`?

I use Bvckup2 on Windows for local (and NAS) backups and Backblaze Backup for
cloud backups. I'm a little surprised at how extremely primitive the "end user"
backup software is on Linux... the headless, do-it-by-hand stuff like `restic`
is fantastic though.

### 2022-11-06

I've got scripts backing up locally, to my NAS, and to B2 via `restic` scripts.
They fire off regularly (hopefully) via user `systemd` units. Not the modern
GUI I was hoping for, and not the flat-fee unlimited backups, but the per-month
fee for B2 is going to be cheaper until I upload my music collection (then it's
roughly the same).

So, I guess I'm using Linux now for my daily driver?

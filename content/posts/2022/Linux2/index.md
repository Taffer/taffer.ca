---
title: "Why didn't I just switch to Linux earlier?"
date: 2022-12-06
draft: false
tags: ["Linux"]
categories: ["Linux"]
aliases: ["/why-didnt-i-just-switch-to-linux-earlier/"]
---

So, I've been using Linux (specifically [Kubuntu](https://kubuntu.org/) 22.10)
for about two months now. When Backblaze sent me an email wondering why it
hadn't seen my Windows system in something like five weeks, I even took the
plunge and nuked my Windows partition.

An aside:

> Backblaze's support folks are fantastic. I'm using B2 with `restic` for
> cloud backups and asked if they could refund me any of my online backup
> license, since it ran 'til March. I'll (eventually) be spending that money on
> B2 storage, but it was nice to get some back from my now-unused Windows
> license.

I'm happier using Linux. If some part of the system is annoying me, I can
usually just... change it. Replace it with something I like better, or tweak
it (KDE is *insanely* tweakable as a desktop environment). My laptop seems
faster, and everything works smoother. I've got a sane development environment
that isn't painful to use. Nobody is slurping up telemetry data and selling
that information to sketchy data brokers.

Here's what my desktop currently looks like:

![A reduced screen shot with a nice background image, toolbar on the left edge, and widgets on the right edge.](featured.png "Chris' desktop, December 2022")

That desktop background is from
[Digital Blasphemy](https://beta.digitalblasphemy.com/), his art is awesome. I
use an app called Variety to set a random background from my collection every
15 minutes.

It's not all "OMG this is completely perfect!" of course. There are a couple
of things that aren't smooth:

* Waking from sleep seems fraught with peril, maybe one time in ten or less.
  I've had the screen freeze, or just stop accepting mouse/keyboard input.
  Presumably there's a process I could restart, but I generally just switch to
  a text console and do a reboot. Or power cycle if things are truly pooched.
  This isn't ideal, but booting takes *significantly* less time that it does
  with Windows, and the time from login to useful desktop is similarly lowered.
* My wifi goes stupid sometimes, and slows down from tens of megabytes per
  second to tens of kilobytes per second. Cycling the wifi connection on/off
  fixes it. I blame Intel's crap software, but who knows. This is another
  danger of waking from sleep.
* The desktop widgets aren't as nice as the
  [Rainmeter](https://www.rainmeter.net/) widgets I was using in Windows. I
  miss [Gadgets](https://github.com/SilverAzide/Gadgets) a little, no lie.

In general though, everything is working better for me. YMMV of course, I've
got friends who love doing development on Windows.

This post was originally
[here](https://codeberg.org/Taffer/Blog/src/branch/main/2022/12-06-Linux.md).

## Related Posts

* [Why don't I just switch to Linux?]({{< ref "/posts/2022/Linux/index.md" >}})

## Updates

### 2022-12-10

The wifi thing is *really irritating*. My laptop has an Intel Wi-Fi 6 AX200
card of some flavour (also irritating: Intel has `MAX_INT` SKUs of every
product) that generally works great.

It seems to have wake-from-sleep issues with the current firmware/software from
Intel.

Under Windows 11, wifi would randomly stop resolving anything via DNS. The
only work-around for this issue was turning the entire laptop off and back on
again, with a ~30 second wait. This was part of the "death by 1000 cuts" that
lead me to trying Linux again.

In Linux, wifi speed will drop from somewhere around 450 Mbps to around 450
Kbps. This generally happens a minute or so *after* waking from sleep. Luckily,
cycling my wifi connection fixes the issue, and only takes a few seconds.

The version of `iwlwifi` currently shipping with Kubuntu 22.10 loads Intel
firmware version `72.daa05125.0`; it actually shipped with version `70`, but I
grabbed the newer one from Intel's website. There are three newer releases of
the firmware... hopefully one of these will address the issue, eventually.

I also looked into upgrading to the newer Intel Wi-Fi 6E AX210, but it didn't
sound helpful unless you've already upgraded your wifi to support 6E. It might
be basically the same part running the same software, but again, Intel has a
billion SKUs of everything and no easy way to compare parts.

I suppose another solution would be to never put my laptop to sleep, but that's
stupid and gives me flashbacks to people carrying half-open Windows laptops
to/from meetings back in the day.

---
title: "OpenSUSE Tumbleweed, Part Two"
date: 2024-03-29T11:03:29-04:00
draft: false
showHero: true
tags: ["Linux", "OpenSUSE", "Upgrades"]
categories: ["Linux"]
---

Another week using OpenSUSE Tumbleweed (and several distro updates via
`zypper dup`), resulting in some solutions to past annoyances, and new
irritations.

## Current Desktop

Looking good!

![My KDE Plasma 6 desktop, with a few widgets: a clock, a CPU monitor, a network monitor, a RAM/storage monitor and a weather forecast.](Screenshot_20240329_115852.png)

For some reason, the Godot 4.2.1 icon keeps moving to the middle of the screen
(which is where I put it for an experiment with the SaveDesktop app mentioned
below). :shrug:

I'll probably move the task bar so it's alone the left edge, but I haven't yet.

## Unresolved Annoyances

Things from the
[previous blog post]({{< ref "/posts/2024/opensuse" >}}) that I'd like
to address in this one:

- I’d like the fingerprint sensor unlock to work more frequently, or to
  understand when it’s usable.
  :arrow_right:
  [https://fprint.freedesktop.org/](<https://fprint.freedesktop.org/>)

<!-- markdownlint-disable MD033 -->
I still don't understand when it's usable. Sometimes I can unlock the desktop
with it. Usually I can unlock 1Password with it. If I open a text console
(<kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>F1</kbd>) I can log in with my fingerprint.
:shrug:
<!-- markdownlint-enable MD033 -->

- Some kind of keyboard lighting control would be great. I like the ridiculous
  rainbow, but I want it to be static, not animated. :arrow_right: Success! See
  below in the Keyboard section.
- I want to completely disable the trackpad when a mouse (Bluetooth in my case)
  is connected. :arrow_right: Does `systemd` trigger things when Bluetooth
  devices are added/removed? Can I use that?

I think `systemd` was a red herring, but I think I might be able to do this
with [`udev` rules](https://wiki.archlinux.org/title/udev#About_udev_rules) if
I can figure out the right rule *and* figure out how to enable/disable the
trackpad from the command-line in KDE. I've found some Gnome instructions, but
nothing for KDE yet.

- I should read up on YaST2 and `zypper`. :arrow_right:
  [https://yast.opensuse.org/](<https://yast.opensuse.org/>) and
  [https://en.opensuse.org/Portal:Zypper](<https://en.opensuse.org/Portal:Zypper>)

Still not entirely sure how Discover in KDE relates; I tend to do "big" changes
via `zypper` to make sure nothing weird happens. Especially times when you
need to `zypper dup` for an OS update.

- I want to use Wayland. I have no idea how to debug the hang because I can’t
  switch to a console. Hopefully an update will fix this soon, but I should
  also try to dig up some info on how to debug the SDDM to Wayland startup
  sequence. :arrow_right: See below in the Wayland section.

## General

- Preferred installation prios are now: native packages, Flatpak, AppImage

For whatever combination of reasons, Flatpak apps work *a lot* better for me
than AppImage apps. I sort of expect them to work/behave the same.

- Wait, Todoist is available as a Flatpak? Why did I Snap it?! Now it works
  properly again, wooo! Removed `snap` and hopefully all of its nonsense.
- Sometimes I open the laptop and it's shut down, but boots without pushing the
  power button.

## Hardware

### Audio

- Mucking with udev (`udevadm trigger` specifically) kills the internal audio.
  I assume I can restart something to bring it back, but I'm not sure what at
  this point… I'm not even sure which audio server is installed.

### Keyboard

- Framework thread:
  [https://community.frame.work/t/responded-openrgb-support/37817/6](<https://community.frame.work/t/responded-openrgb-support/37817/6>)
- This fork of QMK apparently lets OpenRGB work with the Framework keyboard:
  [https://github.com/tagno25/qmk\_firmware](<https://github.com/tagno25/qmk_firmware>)
- So, if I can figure out how to build that, I need to install a different keyboard firmware?! Exciting!

I bailed on this because I don't want to flash it with my own custom firmware.

- [https://community.frame.work/t/fedora-kde-disable-while-typing-for-touchpad-doesnt-work/47418/5](<https://community.frame.work/t/fedora-kde-disable-while-typing-for-touchpad-doesnt-work/47418/5>)
  might be able to add a quirks file for the FW16 keyboard… what would it
  change/improve?
- Supposedly [keyboard.frame.work](https://keyboard.frame.work) will let you
  configure things (keymapping, backlight, etc.); why doesn't it work from
  OpenSUSE? :arrow_right: Ah-ha, PEBCAK, the QMK rules file from the URL above
  works properly if you name it correctly (mine was `.txt` due to duh), was
  able to use the website to adjust the keyboard effects via Chromium:
  [qmk\_firmware/util/udev/50-qmk.rules at master · qmk/qmk\_firmware](<https://github.com/qmk/qmk_firmware/tree/master/util/udev/50-qmk.rules>)

Mildly annoying that I have to use Chromium for that, but I do agree with
Firefox's decision to not allow web sites direct hardware access. That seems
like a very exploitable feature if there are bugs in the browser code…

### Printers

- In the General Configuration tab for my wifi settings, I noticed a "Firewall
  Zone" setting, which was blank.
- It's a drop-down of the defined firewall zones; I set this to "home" (the
  "internal" and "trusted" zones might also be interesting; I assumed "home"
  was more secure than "trusted", which is probably good thanks to IoT being so
  terrible). **Update:** This also lets me `ssh` to the system, so maybe I can
  debug my Wayland session issue.
- KDE Printer setup was now able to see the printers, but couldn't add them.
- Tried YaST2 and was able to set up the printers! They also show up right away
  in the KDE Printer setup, which I'd left running…
- That used the wrong driver apparently; installed the "Manufacturer's PPD"
  package, which let me pick the suggested PostScript driver. Victory!
- Still haven't tried the photo printer. We don't use it often, and I wanted
  to try digging up the ICC profiles for it.

### Trackpad

- Hmm, I wonder if there's a `systemd` Bluetooth event or something when a
  mouse is connected or disconnected? Then I'd just need some way to toggle the
  Trackpad.
- Per this thread
  [https://community.frame.work/t/fedora-kde-disable-while-typing-for-touchpad-doesnt-work/47418/11](<https://community.frame.work/t/fedora-kde-disable-while-typing-for-touchpad-doesnt-work/47418/11>)
  you can make it work by adding a `libinput` quirks file and restarting; didn't
  help for me.

I'm now investigating using `udev` rules to do this, but haven't been able to
figure out the right way yet.

### Screen

- notebookcheck has a link to an ICC profile in their review:
  [https://www.notebookcheck.net/Radeon-RX-7700S-performance-debut-Framework-Laptop-16-review.790807.0.html](<https://www.notebookcheck.net/Radeon-RX-7700S-performance-debut-Framework-Laptop-16-review.790807.0.html>)
- easy to install in Wayland, not even going to try with X11.

I mean, I might try with X11, but I'd really rather use Wayland.

## Wayland

- The firewall zone setting might be the missing piece so I can `ssh` in and
  try to see why it hangs when I tried to log in with a Plasma (Wayland)
  session.
- When I try to launch Plasma (Wayland), there's an Xorg.bin process that
  starts; killing it brings me back to the SDDM screen. I have no idea if
  this means anything.
- If I log in via console and `dbus-run-session startplasma-wayland` I'm
  instantly brought to a Wayland version of the Plasma desktop. There are no
  errors in any of the logs. :shrug:
- This somehow broke the Todoist `snap` package; no errors or anything, just
  fails to start. Starting from a terminal gives the extremely helpful
  `/user.slice/user-1000.slice/session-17.scope is not a snap cgroup` message
  and re-installing the `snap` didn't fix it.
- `@danil@mastodon.gamedev.place` suggested checking if `xf86-video-amdgpu` was
  installed; it wasn't, but installing it didn't help either.
- There's something dying/failing/hanging in the SDDM -> Plasma (Wayland)
  hand-off, and it doesn't leave anything in the system logs (via `dmesg`). Not
  sure where else to look. SDDM doesn't have debug/verbose/log level settings,
  and I haven't figured out where to start poking KDE's startup process for
  information.
- Another Tumbleweed snapshot today, Wayland still broken. Definitely going to
  have to debug this somehow, 'cause I want to use Wayland. **Update:** There
  was another `zypper dup`-requiring update the next day, still no joy for
  Wayland.

## KDE Plasma 6

- Steam doesn't respect the "Global Scale" display setting in KDE, which isn't
  really surprising. And it doesn't seem to have a zoom setting. Such tiny
  text… **Fixed?** Steam used to have a way to zoom the UI; currently making
  the window larger seems to stretch it. Big Picture Mode is also larger.
- Discord can't display Japanese/Chinese text, I'm getting the Unicode "no
  glyph" rectangle. I'm not sure how to figure out what font it's using, so I'm
  not sure how to fix this. **Fixed:** the Noto CJK fonts weren't installed.
- Hey, Discord's start-up behaviour is set to load it when I log in, but it
  doesn't.
- The Discord icon is only correct when it's running, then it switches back to
  a default document icon? **Fixed:** installed the Flatpak version instead and
  everything is happy.
- [SaveDesktop](<https://github.com/vikdevelop/SaveDesktop>) lets you
  save/restore your desktop config. Seems to work for KDE widgets, but not
  desktop icon locations. Why isn't this function built into the desktop
  environments so I don't have to log out/in to pick up the changes?
- In my console-launched Wayland session, there's some sort of seconds-long lag
  between clicking a link in Thunderbird, and having the tab open in Firefox.
  It frequently results in me clicking several times. It's significantly faster
  for me to copy the URL, switch apps, open a new tab, and paste the URL. This
  delay is *much* shorter in Plasma (X11).
<!-- markdownlint-disable MD033 -->
- Similarly, there's a noticeable delay when I press
  <kbd>Alt</kbd>+<kbd>Tab</kbd> to switch between apps. I wonder if there are
  some logging/debugging settings cranked up due to Plasma 6 being so new?
<!-- markdownlint-enable MD033 -->
- I have Konsole set to start up with a 132x50 terminal inside. It always comes
  up at 132x49.5-ish, even though I adjust it every time.

## Continued…

I'm going to keep plugging away at these things until I've got everything set
up *just* the way I want it. Or I get super-frustrated and reinstall the
system using OpenSUSE Leap (and then I'll probably start complaining about
out-of-date software).

Things I'll (hopefully) figure out this week:

- Some way to use `udev` rules to recognize when my Bluetooth mouse is
  added/removed from the system.
- Some command-line way to enable/disable the trackpad.
- The right ICC profile for our colour printer.
- *Maybe* get X11 using the screen ICC profile.
- Debug the SDDM to Wayland startup issue. :crossed_fingers:

{{< alert "bomb" >}}
Somehow this is *still* less irritating for me than just using Windows 11
day-to-day.
{{< /alert >}}

## Related Posts

- [OpenSUSE Tumbleweed]({{< ref "/posts/2024/opensuse" >}})
- [Framework 16]({{< ref "/posts/2024/framework16" >}})
- [Why don't I just switch to Linux?]({{< ref "/posts/2022/Linux" >}})
- [Why didn't I just switch to Linux earlier?]({{< ref "/posts/2022/Linux2" >}})
- [Upgrading Kubuntu 22.10 to 23.04]({{< ref "/posts/2023/Linux" >}})

---
title: "Hashtags Hugo"
date: 2024-02-27T17:31:14-05:00
draft: false
tags: ["Hugo", "howto"]
categories: ["Website"]
---

If this works, I figured out how to add a post's tags to the RSS feed's
`<description>` block.

## Who did what now?

On my site, [Hugo](https://gohugo.io/) automatically generates an RSS feed
from the blog posts. You can see it [here](https://taffer.ca/posts/index.xml)
(use that URL to subscribe to the RSS feed, if you want to get all of my posts
delivered into your favourite RSS feed reader for some reason).

Then, the lovely [MastoFeed](https://mastofeed.org/) notices when a new post
is added to the feed, and re-posts the information to
[my Mastodon account](https://mastodon.gamedev.place/@Taffer).

MastoFeed is really simple, and only gives you a couple of knobs to control
how the post appears on Mastodon. I've got mine set like this:

```
New post - (TITLE) (LINK)

(SUMMARY)
```

When it posts to Mastodon, it shows "New post -" followed by the title of the
blog post, a link to it, and the summary (whatever is in the RSS feed's
`<description>` block). Perfect!

Except, by default, the RSS feed `<description>` doesn't include the post's
tags…

Tags are *very important* for discoverability on Mastodon.

## Hugo template-o-rama

Hugo's super flexible, and lets you set up templates for everything. In my
case, I'm also using this [Blowfish](https://blowfish.page/) theme, which
provides an `rss.xml` template.

To modify the `rss.xml` for my own purposes, I copied it into my site's
`layouts/_default` directory and opened up the copy in my favourite text
editor (currently [Sublime Text](https://www.sublimetext.com/), if you care).

There's a lot going on inside the `rss.xml`, especially if you're not
familiar with Hugo's templating language:

```xml
{{- $pctx := . -}}
{{- if .IsHome -}}{{ $pctx = .Site }}{{- end -}}
{{- $pages := slice -}}
{{- if or $.IsHome $.IsSection -}}
{{- $pages = $pctx.RegularPages -}}
{{- else -}}
{{- $pages = $pctx.Pages -}}
{{- end -}}
{{- $limit := .Site.Config.Services.RSS.Limit -}}
{{- if ge $limit 1 -}}
{{- $pages = $pages | first $limit -}}
{{- end -}}
{{- printf "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?>" | safeHTML }}
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
…
```

That's a lot before we even get to the RSS feed!

Scroll down a bit more, until you find the `<item>` tag definition. In
Blowfish, it looks like this:

```xml
    <item>
      <title>{{ .Title }}</title>
      <link>{{ .Permalink }}</link>
      …
```

A bit below that is the `<description>`, which is going to get filled with an
HTML-ized version of the post summary, which is the first paragraph:

```xml
      <description>{{ .Summary | html }}</description>
```

I've added a second line here (and a couple of newlines; they're not
important) that does a bunch of things:

- If the page parameters has a "tags" section,
- `sort` the tags,
- `apply` the `fmt.Print` function to turn the tags into hashtags,
- stick them together with a space in between (`delimit`).

```xml
      <description>
        {{ .Summary | html }}
        {{ if isset .Params "tags" -}}{{ delimit (apply (sort .Params.tags) "fmt.Print" "#" ".") " " }}{{- end}}
      </description>
```

I had originally included the page category as a hashtag, but that didn't seem
semantically correct, so I simplified it to just the tags.

Time to regenerate my site and hope MastoFeed does what I wanted!

**Update:** It worked! :thumbsup:

---
title: "Passwords"
date: 2024-07-06T08:08:45-04:00
draft: false
showHero: true
tags: ["Passwords"]
categories: ["Security"]
---

A bit of a rant, with citations, about stupid password policies.

{{< alert icon="fire">}}
I got the "hacker" image from a
[Kagi search](https://kagi.com/images?q=creative+commons+hacker&license=share)
for "Creative Commons Hacker" and a "shareable" license; please let me know
if it's *not* something people can re-use and I'll happily swap it out for
something else.
{{< /alert >}}

## The Problem

For decades, IT organizations that don't understand passwords and encryption
have been foisting policies on us along these lines:

- some minimum length (this is good)
- some maximum length (this is stupid)
- upper-case, lower-case, numbers, punctuation (not terrible)
- expires after *n* days (this is *extremely* stupid)

The maximum length is stupid because the *best* way to make a strong password
is just to make it longer. Even if you're not going crazy mixing up upper-,
lower-, numbers, and punctuation, every character you add to your password
makes it much harder for adversaries to brute-force.

Having a maximum length, especially when it's combined with a weird list of
forbidden "special" characters, makes me *really* suspicious that the password
is being stored directly in a database, and that's *exponentially stupid*.
Consult a local security expert if you ever feel like storing passwords
directly *anywhere*.

The ultimately stupid requirement is having passwords that expire. This is a
misguided attempt to mitigate passwords ending up in the
[data breaches](https://haveibeenpwned.com/) that are constantly happening,
and to mitigate old-school problems like people writing their passwords down
on Post-It notes stuck to the bottom of their keyboards. Or the sides of their
monitors.

Instead, this encourages bad behaviour such as:

- Incrementing a number on the end of the existing password: `hunter1` becomes
  `hunter2` and it's just as secure as it was before the change.
- People forgetting their new passwords constantly, which increases the burden
  on front-line tech support who will have to reset the user's password,
  starting the process fresh. Passwords always seem to expire on Fridays…
- Writing down your new password and putting it on a Post-It attached to
  your monitor.

## The Solution

The solution to bad passwords that you need to type frequently:

- Use a longer password, like a passphrase; that's where you write out a
  series of (hopefully) unconnected words, like the famous
  `correct-horse-battery-staple`.
- Never expire passwords unless they actually show up in a breach.

The solution for every other password:

- Use a password manager protected by a nice long passphrase.
- Have the password generate a completely random, long password that's
  unique for every website, app, etc. I tend to use 32 characters
  (upper-, lower-, numbers, punctuation) so it's not insane for those rare
  occasions where I *do* need to type them in, generally on systems that
  don't support my password manager (things like game consoles or streaming
  devices, for example).

The passphrase for your password manager is then the only thing you ever have
to remember.

On the server side, there needs to be a few things happening:

- Throttle login attempts so an adversary can't keep trying until something
  works.
- Lock out accounts that have too many failed login attempts. This can unlock
  automatically after some time, or an admin can unlock it.
- Seriously, do not store passwords. Ask a security expert; they should be
  suggesting something that sounds like "hash and salt", possibly with
  `scrypt()` thrown in there.

### Bonus Points

Want your accounts to be even more secure? Of course you do!

- Use a good password manager. There are lots, so picking a good one might
  be annoying. A few things to know:
  - Don't use one that's tied to a specific OS or mobile platform; you never
    know when you might need to switch, or access your passwords from something
    else.
  - If it stores passwords in the cloud, you'll want to be able to control
    that data somehow. Can you pick the cloud and use your own account there?
    Does it claim to encrypt things properly so the cloud data can't be
    compromised when it's downloaded by an adversary?
  - A good reputation is important. Do the company or people behind it
    publish whitepapers about their security model? Do they have a good rep
    amongst your local security experts? Have they been in the news repeatedly
    for data breaches or security problems?
- If an account supports second-factor authentication (2FA), *enable it*:
  - Texting code to your phone isn't *great*, but it's better than nothing.
  - Most password managers support storing these (usually called TOTP or OTP,
    one-time passwords) along with your password. This sort of defeats the
    idea of having a *second* thing, but it makes 2FA *much more usable*,
    which encourages people to use it.
  - Be sure to download any "recovery codes" the account provides, usually
    when you're setting up 2FA. You'll need these if your code generator dies
    somehow.
- Change any weak or compromised passwords you've currently got:
  - Many password managers have a "health check" or something that will go
    through your password collection and flag any that need changing.
  - [Have I Been Pwned?](https://haveibeenpwned.com/) is the canonical
    website for checking your accounts; just enter your email address and
    it'll warn you about any breaches that included your information.

My current password manager favourites are:

- [1Password](https://1password.com/) - These folks have a great reputation,
  their software supports everything I use, and it's really convenient for me
  to have my passwords in the cloud (but still, hopefully, secure). Having a
  shared password "vault" with my family is also really handy.
- [BitWarden](https://bitwarden.com/) - Also a great choice, I chose 1Password
  over BitWarden based on a very mild preference for 1Password's UI. Their
  client is open source so (theoretically) people can analyze the code and
  make sure they're not doing anything wrong. There's also an unofficial
  server ([Vaultwarden](https://github.com/dani-garcia/vaultwarden)) if you
  want to use the cloud via your own server.
- [KeePassXC](https://keepassxc.org/) - For people that want full control over
  where their password data is stored, without relying on someone else's
  security. Another open source project, but without a company behind it.

## The Evidence

Literally every cryptography expert will tell you that longer passwords are
better, but what else is there?

- A nice writeup from the British government's security experts:
  <https://cesgdigital.blog.gov.uk/2015/09/08/making-security-better-passwords/>
- Another one from their NCSA experts:
  <https://www.ncsc.gov.uk/articles/problems-forcing-regular-password-expiry>
- One from Microsoft (written before they forgot how to do security in Azure):
  [Microsoft Password Guidance](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/06/Microsoft_Password_Guidance-1.pdf)
- The Windows 10 security baseline:
  <https://blogs.technet.microsoft.com/secguide/2019/05/23/security-baseline-final-for-windows-10-v1903-and-windows-server-v1903/>
- The Canadian government's security experts:
  <https://www.canada.ca/en/government/system/digital-government/password-guidance.html>
- *A Canonical Password Strength Measure* by Eugene Panferov
  ([arXiv:1505.05090](http://arxiv.org/abs/1505.05090))
- XKCD's famous comic: <https://imgs.xkcd.com/comics/password_strength.png>
- Another article:
  <https://cybernb.ca/en/blog/2017/12/01/webinar-recap-proactively-protecting-your-business-against-cyber-threat/>

Here's some guidance about how many failed login attempts should lock your
account:

- NIST: see section 5.2.2 of
  <https://pages.nist.gov/800-63-3/sp800-63b.html#sec5>
- Microsoft (when they knew how to do security): see the "Countermeasure"
  section of
  <https://docs.microsoft.com/en-us/windows/security/threat-protection/security-policy-settings/account-lockout-threshold>
- Another one from Microsoft:
  <https://blogs.technet.microsoft.com/abizerh/2013/04/21/how-an-incorrectly-configured-account-lockout-policy-can-give-more-pain-than-security/>

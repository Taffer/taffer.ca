---
title: "Editors"
date: 2024-03-19T06:50:44-04:00
draft: false
showHero: true
tags: ["Linux", "Gamedev", "Tools"]
categories: ["Gamedev", "Linux", "Dev"]
---

One of the *things I do every year or so* is re-evaluate my tools. I'm a bit
of a neophile, and I like to know what's out there. Text editors/IDEs are one
of those things, because I don't want to just give up and use `vim`.

(Joke! I've known just enough `vi`/`vim` since the early 90s to get around.
It's almost always there on UNIXy systems, and it's handy for configuring a
better editor…)

Also, my friends were giving me grief for being a Microsoft hater because I
wasn't willing to check out
[Visual Studio Code](https://code.visualstudio.com/), mostly due to worries
about telemetry and accidentally contributing to their idiot LLMs somehow.

## Aside - Hating Microsoft

As a *long* time computer user and technologist, I've spent considerable time
using almost everything (although I missed out on Amiga and Acorn somehow).
It's not "just" Microsoft; everything is annoying and has given me lots of
stress and irritation over the years.

I do hate Microsoft though. They've steam-rollered so many better technologies,
and they've made all of their software worse (IMHO, your opinion may vary)
since the Windows 7 era.

I do miss the Windows 7 era OneNote though, that was an amazing application
until it was updated to remove half the features and make the UI terrible.

## Editors and IDEs

I don't normally use IDEs unless it's necessary. They always seem too rigid,
and I don't like having things "forcing" me to work a particular way. Also,
Xcode once kicked my dog, so that's right out.

Usually, I want a nice text editor that supports things like syntax
highlighting, linting, spell-checking (yeah, even for code), and that generally
lets me work the way I want to. In the past I've used EMACS (and a bunch of
lightweight clones, like MicroEMACS, Jove, and Jed), and weird things like
Eddie on BeOS and whatever almost-a-text-editor/almost-a-simple-word-processor
I had on the Atari ST.

For the past seven? years, I've been using
[Sublime Text](https://www.sublimetext.com/index2), and it hits all of my text
buttons nicely, but it doesn't seem to be getting much development attention
these days. It's not really missing anything big, and it's really stable, so
this isn't a big deal, really.

But, like I said, I like to look around, and I like my tools to be under
active development. Open Source of some flavour would also be nice.

## CudaText

I saw a post on Mastodon that [CudaText](https://cudatext.github.io/) had been
updated. It's an open source editor with features similar to Sublime, written
in Pascal (which is irrelevant, but interesting).

I couldn't get it to run on Linux. :shrug: I didn't try on macOS.

## Visual Studio Code

I found some settings to
[disable telemetry](https://gist.github.com/hyperupcall/99e355405611be6c4e0a38b6e3e8aad0),
so why not, I'll try [Visual Studio Code](https://code.visualstudio.com/).

Launched it at work, added those settings, and started mucking around and
configuring things. There are a *lot* of configuration options, which aren't
necessarily documented in a useful way, but I was able to get things the way
I like them (*i.e.* the way I had Sublime set up) pretty quickly, *and* a
version of my favourite Sublime theme (Sunburst) was available.

There isn't really anything interesting in my settings (which is good), but
I'll describe my extensions and key bindings.

### Extensions

I generally try to install as few add-ons/extensions as possible; here's what
I've added to VSCode:

* .NET Install Tool - OK, *I* didn't install this, and I'm not sure it even
  does something useful on macOS and Linux, but I'm mildly afraid to uninstall
  it in case it breaks something with VSCode or one of the other extensions.
  I don't do .NET development these days. It's *possible* the Godot extension
  added it, since C# is supported there.
* C/C++, C/C++ Extension Pack, C++ Themes - I do C sometimes.
* CMake, CMake Language Support, CMake Tools - When I do C, I like to use
  `cmake`. Maybe like is a strong word? It's better than `make` and `scons`.
* Code Spell Checker and Canadian English Dictionary - My spelling is
  traditionally terrible, so here we are. This attempts to be language-aware
  too, so you can get some useful spell-checking while programming.
* Even Better TOML - Useful but awful alternative to JSON.
* Fish - My favourite shell! Useful when I edit scripts and config files.
* Go - I'm learning Go.
* godot-tools - Godot game engine support, in case I want to try using VSCode
  as an external editor. The internal editor is fine, but I'm foreseeing a
  future where my projects are big enough that it's unmanageable without
  additional editor power.
* Hugo Language and Syntax Support - Hugo's an awesome static website
  generator that I've been using.
* markdownlint - Make sure my Markdown is good. I write a lot of Markdown
  these days.
* Pylance, Python, Python Debugger, Flake8 - Python support. I *like* Python.
* Ruby LSP - I've been meaning to learn Ruby, since it keeps coming up.
* Rust Syntax, rust-analyzer, crates - I'm learning Rust.
* Sunburst Theme - An excellent dark theme that I find very readable.
* YAML - Yet another useful but awful alternative to JSON.

### Key Bindings

Thanks to decades of muscle memory, there are some key bindings I always set
up in every editor: EMACS-style Ctrl+A, Ctrl+E, Ctrl+K and Ctrl+Y, and then a
replacement for Select All, since that's usually Ctrl+A. By default, Ctrl+K was
set to "delete to end of line", which is only half way there.

Here's my set of key bindings, very simple:

```json
// Place your key bindings in this file to override the defaults
[
    {
        "key": "ctrl+e",
        "command": "cursorLineEnd"
    },
    {
        "key": "ctrl+a",
        "command": "cursorLineStart"
    },
    {
        "key": "ctrl+shift+a",
        "command": "editor.action.selectAll"
    },
    {
        "key": "ctrl+y",
        "command": "editor.action.clipboardPasteAction"
    },
    {
        "key": "ctrl+k",
        "command": "runCommands",
        "args": {
            "commands": [
                "cursorEndSelect",
                "editor.action.clipboardCutAction"
            ]
        },
        "when": "textInputFocus && !editorReadonly"
    }
]
```

## A Weekend of Hacking

I spent some time on Friday afternoon using VSCode, and a weekend working on
my website and a Godot/Docker/Codeberg CI demo.

It's working well, and I actually like the interface. I don't think I'm quite
ready to uninstall Sublime, but I'm going to keep using VSCode this week and
see if I run into any pain points.

## XBox Huge

Another reason I didn't want to give VSCode a chance was Electron; I hate how
massive it makes everything, and I've got several "must have" Electron apps
running already. On the other hand, I have lots of RAM, so why worry?

On Linux right now, VSCode is apparently using somewhere between 1.1GB and
700MB of RAM, which is absolutely insane for a text editor. It seems to
fluctuate based on what I'm doing.

Sublime Text is using 88MB of RAM. :shrug:

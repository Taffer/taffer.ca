---
title: "Remote Work, 2024 Edition"
date: 2024-06-19T06:41:36-04:00
draft: false
showHero: true
tags: ["Remote", "WFH"]
categories: ["Work"]
---

A collection of job search sites and specific companies that are remote-first
or remote-friendly, to help everyone be happier with their work.

Last year, I [made a post](/posts/2023/remote/) to help people find jobs that
don't contribute to the climate crisis by forcing them to commute to an
office and expose themselves to COVID-19 and other airborne diseases. I felt
a need to re-organize and clean up, so here's a fresh new edition for people
looking for work in 2024.

{{< alert >}}
**Warning:** The companies listed in
[this Google doc](https://docs.google.com/spreadsheets/d/1nW7kbqVz8XUCRFEgH5Y60YzmoOfl7IM9w4DAhb2kcX4/edit?gid=0#gid=0)
have apparently been posting "ghost jobs". These are job postings where there's
no intent to hire. There are various sketchy reasons for doing this, and none
of those reasons help *you* in any way.
{{< /alert >}}

**Last updates:** 2024-11-23

## Still Searching

There are *lots* of companies of all sizes doing remote work who are giddy with
excitement watching the FAANGs bend over for commercial real estate interests
and 50s-style "management by walking around".

Let me know if I'm missing any of these:

* Job sites or search engines for remote work.
* Companies who are remote-first, or remote-friendly.
* Companies who *used to* do remote work, but who have returned to offices;
  I'll remove them ASAP.

You can contact me through an
[Issue](https://codeberg.org/Taffer/taffer.ca/issues) on CodeBerg, via
[email](mailto:chrish@pobox.com), or on
[Mastodon](https://mastodon.gamedev.place/@Taffer). I'm `the_real_taffer`
🙄 on Discord if that's your thing, just remember to give me some context
before dumping data.

**NOTE:**  I'll keep updating this as long as I keep getting new information,
or until I make a new edition!

Let me know if I've gotten anyone in the wrong buckets here! Or if I've
gotten the wrong one with my Glassdoor links. There sure are a lot of companies
with very similar names…

## Job Search

Job-finding sites specializing in remote work:

* [Arc.dev](https://arc.dev/) - in 2024-11 they had a
  [blog post](https://arc.dev/talent-blog/remote-first-companies/) about
  remote-first companies
* [AwwCor.Inc](https://awwcor.com/jobboard)
* [Communitech](https://www1.communitech.ca/jobs) (their "Remote" filter may
  include "Hybrid" though, so watch out)
* [crossover.com](https://www.crossover.com/)
* [findasync.com](https://findasync.com/)
* [Flexjobs](https://www.flexjobs.com/remote-jobs) (not useful without a paid
  account, with a monthly fee) - Be sure to check anything you find here on
  Glassdoor; they listed at least one
  ([Clio](https://www.glassdoor.ca/Reviews/Clio-Reviews-E713360.htm)) who've
  done a hard RTO.
* [Funded.club](https://funded.club/startupjobs)
* [HiringCafe](https://hiring.cafe/)
* [Jobgether](https://jobgether.com/search-offers)
* [nodesk.co](https://nodesk.co/)
* [OpenSource Job Hub](https://opensourcejobhub.com/) is specifically for jobs
  focusing on open source work, many of which are remote.
* [Remote.co](https://remote.co/)
* [remote.io](https://remote.io/)
* [remoteintech.company](https://remoteintech.company/)'s *really* long list;
  not sure how up-to-date this is, as it lists Amazon.
* [Remotive](https://remotive.com/remote-companies) (the list directly
  below this is mostly an ad for this job site; they've got a membership
  with a one-time fee)
* [Remotive's list](https://docs.google.com/spreadsheets/d/18bljq3y5YTxPLmA4xj10EaKxs1KWhIdLTr8bF1K5XKI/edit?pli=1&gid=0#gid=0);
  this is a Google Doc with about 170 companies listed.
* [Virtustant](https://jobs.virtustant.com/)
* [weworkremotely.com](https://weworkremotely.com/)
* [workingnomads.com](https://www.workingnomads.com/jobs)
* [Yanir Seroussi's great list](https://github.com/yanirs/established-remote);
  this has a ton of useful information (business/product, tech stack, etc.)
  that I'll never have time to add to my list.

{{< alert "mastodon" >}}
[This Mastodon post](https://toots.niark.nexus/@qaqelol/112650048406008215)
mentioned <https://workadventu.re/>, which looks like a really cool virtual
office sort of thing, an open source version of
[Gather](https://www.gather.town/). I'd love to try these out!
{{< /alert >}}

### B Corp Jobs

The B Corp certification is apparently for "beneficial" (thus, the *B*)
companies:

> Certified B Corporations are businesses that meet the highest standards of
> verified social and environmental performance, public transparency, and legal
> accountability to balance profit and purpose.

A list of Canadian certified B Corp companies:
[https://bcorpdirectory.ca/explore/](https://bcorpdirectory.ca/explore/)

A list of American certified B Corp companies:
[https://www.bcorporation.net/en-us/find-a-b-corp/](https://www.bcorporation.net/en-us/find-a-b-corp/)

These seem more inclined to support remote work, and to generally not be
awful.

### Green Jobs

These three job search sites specialize in "green" companies; I haven't verified
that they're all remote, but I'd expect that to be a strong possibility:

* [MCJ Collective](https://www.mcjcollective.com/)
* [Terra.do](https://terra.do/)
* [Work On Climate](https://workonclimate.org/)

### Charity Jobs

Here's a job search for charities and other nonprofits; not necessarily
remote, but still a good thing:

* [CharityVillage](https://charityvillage.com/)

## Remote Companies

I've organized the list of companies into two buckets, "Remote-First" and
"Remote-Friendly":

* "Remote-First" are mostly companies that don't have traditional offices.
* "Remote-Friendly" are companies that have offices and usually offer in-office
  or "hybrid" positions (2-3 days/week in office, usually) as well as fully
  remote.

Some positions, usually for unicorn applicants (that is, people who have
really specialized skills and experience), can be *made* remote, even at
old fashioned companies. But don't count on it.

**NOTE:** The ℹ️ is a link to Glassdoor's reviews for the company. You may have
to log in to view them, and Glassdoor may be up to some
[shady things](https://arstechnica.com/tech-policy/2024/03/glassdoor-adding-users-real-names-job-info-to-profiles-without-consent/).

**Late November 2024 Updates:** I've added a ton of new companies from a
[thread on LinkedIn](https://www.linkedin.com/posts/christineorchard_remote-remotework-workfromanywhere-activity-7262301434522554368-H0F9/),
but I *might* have them in the wrong categories (I imagine most of them belong
in the "Remote-Friendly" list). I didn't have time/energy to slog through all
of their websites to figure it out, so please let me know where I've gotten it
wrong!

### Remote-First Companies

These companies are remote-first. If they're *not*, please
[let me know](mailto:chrish@pobox.com?Subject=Remote-First%20Correction)!

{{< company-list "fully-remote.json" >}}

### Remote-Friendly Companies

These companies are (currently) remote friendly, but you might have to be
careful when searching and applying. Many positions are "hybrid" rather than
fully-remote, and require 2-3 days/week in office.

Please
[let me know](mailto:chrish@pobox.com?Subject=Remote-Friendly%20Correction)
if any of these belong in the **Remote-First** collection!

{{< company-list "remote-friendly.json" >}}

## Related Posts

* [Remote Jobs (2023 Edition)](../../2023/remote/)

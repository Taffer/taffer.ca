---
title: "Nested repos"
date: 2024-01-15
draft: false
tags: ["git"]
categories: ["Dev", "Gamedev"]
aliases: ["/nested-repos/"]
---

I've once again been thinking about `git submodules` and how I might be able to
use them when making games. My requirement is to have my open source game, but
potentially use commercial assets inside *without* exposing those assets in a
public `git` repo.

> I also took a side quest to look at Mercurial, since the "best" source control
  systems for game development are stupidly expensive. Mercurial seems like it
  would have the same or similar problems that `git` has... poor handling of
  binary files, with large binary files being particularly troublesome.

## submodules Solution

The right? solution to this is to use `git` submodules; you essentially create
(or check out) the commercial assets repo inside of your code repo, and use
various `git submodule` commands to keep things sane.

Atlassian has a nice tutorial
[about `git` submodules](https://www.atlassian.com/git/tutorials/git-submodule).

After you've checked out the module-with-submodules repo correctly (or fixed it
after you did it wrong), you can work with files in the main repo, or the sub
repo, fairly normally, and the main repo keeps track of what revision of the
sub repo it's sync'd with.

For whatever reasons, I've always found this awkward and error-prone. I've got a
mental block around this, and haven't used it enough for it to become second
nature at all.

## Another Solution?

I had an idea this afternoon and it seems sort-of simpler, at least when you
control both repos.

Nest 'em, and tell the main repo to ignore the sub-repo:

```sh
$ git clone some-repo
$ cd some-repo
$ git clone another
$ echo 'another' >> .gitignore
```

Now you've got `another` inside `some-repo`, and they're just normal `git`
repos. You work with them normally. No additional commands, no additional
learning curve. You *do* need to clone the second repo using an extra command
(using submodules you can get both repos using the `--recurse-submodules`
option to `git clone` *if you know to use it on the repo*).

You'd have to be careful about which revision you keep `another` repo at if
you're not in control of it, but I think I'm going to try this when I start
using commercial assets in one of my game projects. The main repo can be
public, commercial assets can go in a private repo, and code/etc. in the main
repo can reference them.

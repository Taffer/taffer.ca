---
title: "OpenSUSE Tumbleweed"
date: 2024-03-23T14:32:45-04:00
draft: false
showHero: true
tags: ["Linux", "OpenSUSE", "Upgrades"]
categories: ["Linux"]
---

After carefully considering my options, I'm trying OpenSUSE Tumbleweed on the
Framework 16.

## But why?

Since giving up on Windows, I've used [Linux Mint](https://linuxmint.com/) and
[Kubuntu](https://kubuntu.org/). I didn't like Kubuntu's over-reliance on
[Snap](https://snapcraft.io/) packages, and their intention to go even further
in that direction.

A few criteria:

* I wanted to try a rolling release if possible, but probably not Arch because
* I want a decent GUI installer
* KDE for the GUI on top of Wayland
* BTRFS for the root filesystem
* Not dependant on Snap packages, I don't like how they behave for desktop apps
* Up-to-date software

I had a short list of "finalists" to check out:

* [OpenSUSE](https://www.opensuse.org/)
* [Fedora](https://fedoraproject.org/)
* [Debian](https://www.debian.org/)
* [Manjaro](https://manjaro.org/)

Fedora would be more attractive if IBM didn't seem to be ruining RedHat. Debian
seems full of outdated software. Manjaro is *almost* Arch with a nice installer,
but they seem to want to head in their own direction… Arch is already a bit of
a niche, so I assume Manjaro would be even teenier. OpenSUSE has a good
reputation and is super-popular in Europe, and they've got a rolling release.

OK, let's give [OpenSUSE Tumbleweed](https://en.opensuse.org/Portal:Tumbleweed)
a try!

![OpenSUSE Tumbleweed logo](Tumbleweed-mix.png)

{{< alert "lightbulb" >}}
I've never used a rolling release before *and* I've never used OpenSUSE on a
desktop/laptop before. A lot of the comments/complaints/nit-picks here might
be due to being clueless! Send me an email or poke me on Mastodon if you've
got a solution or suggestion and I'll be happy to update this post!
{{< /alert >}}

## All the Details

I've been keeping random notes as I installed the OS, set up the software I
like to use, and mucked around getting things set up. I'll try to group them
into related sections.

### Installation

Installation was pretty smooth and straight-forward:

* Default EFI partition is tiny, making it larger was annoying (you have to
  resize the root partition, move it, recreate the EFI partition, then
  adjust/resize the root partition again). The default is 512MB, which the
  [Arch wiki suggests](https://wiki.archlinux.org/title/EFI_system_partition)
  1GB (or 4GB if you have lots of space and are worried about the boot
  partition filling up). I set mine to 2GB. :shrug:

{{< alert "lightbulb" >}}
The [Arch wiki](https://wiki.archlinux.org/) is an amazing resource for current
and detailed information, the people maintaining this are doing an incredible
job!
{{< /alert >}}

* So many things to install! Nice to have current software though. In order of
  preference, I'll be installing native packages,
  [Flatpak](https://flatpak.org/), and finally
  [AppImage](https://appimage.org/).

### Hardware Support

The power button on a Framework 16 is also a fingerprint sensor.

* Can add fingerprints! But can't seem to unlock anything with them, so I think
  I'm missing something.
* `sudo pam-config --update --fprintd` says `pam_fprintd.so` isn't installed…
  so where is it? oh, `pam-config` is looking for `/lib/security` not
  `/lib64/security`, confusing; but I just got to use my finger when the screen
  went to sleep? chaos!
* I can log in to the console with a fingerprint, but not into Plasma; ok, that
  only worked once, but I've been able to wake the screen sometimes and unlock
  1Password sometimes, with my finger.

I *think* there's a short-ish period of time where the fingerprint sensor
works, usually after the screen has gone to sleep. I got to use it *once* for
a `sudo`. 1Password often lets me use the fingerprint sensor to unlock, but
again, sometimes I have to enter my login password.  At this point, I don't
know if it's something weird in the setup, something buggy in Tumbleweed's
versions of everything, or if it's working as expected.

I opted for the `l33t` gamer RGB keyboard, which defaults to showing a rainbow
that cycles from left to right. I haven't been able to figure out what combo
of software I need to make something like [OpenRGB](https://openrgb.org/) work
for it yet.

* Can't get the QMK stuff to work;
  [keyboard.frame.work](https://keyboard.frame.work) doesn't work in Firefox
  anyway?
* Couldn't figure out how to get Chromium to have permission to access the
  keyboard hardware for keyboard.frame.work either.

The Framework 16 comes with a MediaTek MT7922 wifi/Bluetooth card, so I've
been hoping it'll behave better than the
[Intel cards]({{< ref "/posts/2023/Wifi" >}}) I was using on my previous
laptop.

* Wifi seems flakey, a Linux classic. (It dropped in the middle of doing a
  network backup, but has been solid since I've updated/restarted the
  system. The system might have been in a weird state between the first boot
  and installing some udpdates.)
* I can't change the name of my wifi connection (which was set to the device
  name, not the network name) due to permissions in the UI; I expect settings
  apps to ask for root when they need it. Update: This worked after
  restarting. :shrug:
* Wifi's been solid since, despite some abuse.

Bluetooth seems to work well (I frequently use a Bluetooth mouse, and sometimes
use some AirPods), except the default behaviour in OpenSUSE seems to be to load
Bluetooth after you've logged in; I prefer to have it active at the graphical
login screen (SDDM).

* Bluetooth didn't reconnect to my mouse after sleeping, a new twist. (This
  was actually the delayed startup after logging in.)
* Hopefully enable Bluetooth properly on boot and make my mouse work
  automatically:
  [Reddit post about Bluetooth on boot](https://www.reddit.com/r/openSUSE/comments/eoozm2/bluetooth_on_boot/)
* that didn't help
* maybe make Bluetooth work right away, added `/etc/bluetooth/main.conf`: per
  [this OpenSUSE forum post](https://forums.opensuse.org/t/login-with-bluetooth-keyboard-sddm-kde-plasma/145506/8)
* Per
  [this Arch Wiki article](https://wiki.archlinux.org/title/Bluetooth#Auto_power-on_after_boot)
  it should start on boot?
* Adding the `/etc/bluetooth/main.conf` file seems to have done the trick.

The Framework trackpad seems pretty good, for a PC laptop. I'm still surprised
at how much worse trackpads on Windows and Linux are than on macOS.

* OMG I need to disable the trackpad while typing it's driving me insane; I set
  up a keyboard shortcut for this.
* Any way to disable trackpad automatically when a Bluetooth mouse is activated?
* I don't know what it is about how I type, but my palms are constantly moving
  the mouse and clicking in random spots. I'm a fast touch-typist, so this
  drives me insane.
* The "Disable while typing" setting doesn't seem to work at all, which was
  also the case with KDE Plasma 5 on my previous laptop.

### Unusual Software

New laptop, new OS, I might as well try a few new bits of software. *Most* of
the software I use is pretty portable, and I've been taking it with me between
systems across Linux/macOS/Windows for ages.

* I couldn't get Kmail to add accounts properly? (Tried my domain, which is
  on Dreamhost, and my Gmail account.)
* trying out Kmail again; the account setup is janky, and it doesn't pull IMAP
  messages after (although maybe that's because I had my settings wrong), I had
  to initiate that by hand, doesn't display/download my inbox?!
* Kmail claimed it had downloaded all of my messages, but it didn't manage to
  download anything from my Inbox. Back to Thunderbird, which always happens
  when I try out a new mail client.
* What's the relationship between YaST2 and Discover? and zypper.
* `zypper` seems pretty good (I keep typing `zupper` though.)
* might have to set screen to 125% due to old eyeballs; that's still better
  than 1920x1080… will that affect 3D stuff? (This seems to have made desktop
  apps better for me, but I haven't had a chance to try any games yet… I don't
  think it will change the rendering though.)

### Usual Software

KDE Plasma 6 is new-ish, but KDE's what I've been using for the last year and
a half… most of the other stuff I use day-to-day is portable and has been with
me for ages.

* [`exa`](https://the.exa.website/) is now
  [`eza`](https://github.com/eza-community/eza), `exa` is
  [abandoned](https://github.com/ogham/exa).

I don't use Docker much, but AFAIK the "right" way is through
[Docker Desktop](https://www.docker.com/products/docker-desktop/).

* Installing Docker Desktop from the `rpm` file complains about missing
  `docker-ce-cli`, and OpenSUSE doesn't know its signature (I could've added
  their PGP public key first, that would've fixed the sig issue); Desktop can't
  start the Docker engine, and the "Sign In" links/buttons don't work… was able
  to install bare `docker` ("docker engine" I think) from `zypper`, and it
  appears to work, but the Desktop app is still toast.
* Uninstalled Docker Desktop, will use the command-line version if I can.

I'm an ADHD person, so having a good to-do list that operates across all of my
systems is important; I've been using [Todoist](https://todoist.com/), which
comes as an AppImage or a Snap for Linux. I tried the AppImage at first…

* Todoist tells me it needs to integrate with the desktop every time it starts,
  but then it goes to the normal window after opening the Todoist website.
* Todoist can't display desktop notifications? Maybe?
* Todoist upgraded its AppImage, and apparently it broke completely; installed
  `snapd` (:cry:) and the Snap version. Works again, wooo.

KDE Plasma 6 was *just* released, so I expect a few issues.

* I lost the trash can and home directory widgets somehow? moved them from
  `~/Desktop` to somewhere else, then back and voila. :shrug:
* Screen dimmed on low battery, didn't brighten again when I plugged in the
  system.
* Updated today (2024-03-21 around 9:15AM Eastern) and Plasma (Wayland)
  sessions don't load; Plasma (X11) works though; the X11 session takes a
  *really* long time to load between SDDM and KDE splash screen though.
* More updates, but Wayland still hangs, Bluetooth not active on boot at SDDM
  (I think I fixed this up in the Bluetooth section), the SDDM to KDE splash
  takes a very long time.
* Discord (installed from their `.tar.gz` archive) in my task bar gets a
  generic document icon for some reason, until I launch it; same with
  Notesnook. And Notesnook gives me an "Invalid desktop file" notification for
  its AppImage?
* And yet, it runs fine and even has a proper icon in the taskbar?
* Alt-Tab switching seems oddly laggy. It was pretty much instant in KDE 5 on
  Kubuntu.

I'm not sure if my arguments with `systemd` are because it's a newer version
than I'm used to (probably) or if OpenSUSE has it set up a little differently.
(I need `kbct` to customize my keyboard at a very low level so I can map
CapsLock to Control and have it work in Wine. I need my critically acclaimed
MMORPG [Final Fantasy XIV](https://www.finalfantasyxiv.com/).)

* `systemd` services I added, and enabled, don't start automatically? changed
  owner to root, maybe that'll help?
* changed the `After=` and `WantedBy=` bits in my `systemd` units, hopefully
  that'll help after a `systemctl daemon-reload`
* after a `zypper dup`, had to `sudo modprobe uinput` before
  [`kbct`](https://github.com/samvel1024/kbct) would work
* added `/etc/modules-load.d/uninput.conf` to hopefully load `uinput` at boot
  per this [Arch Wiki article](https://wiki.archlinux.org/title/Kernel_module)
  Victory!
* changed mount units again, added automount units per this
  [Manjaro forum post](https://forum.manjaro.org/t/root-tip-how-to-systemd-mount-unit-samples/1191)
* Still no joy. Whoops, added an invalid option to the `cifs` mounts; fixed it
  and it seems to automount properly!
* Need to tweak NAS backup so it waits for NAS automount.

## Unresolved Annoyances

You probably skimmed all that nonsense, so here's a summary of the things still
bugging me:

* I'd like the fingerprint sensor unlock to work more frequently, or to
  understand when it's usable.
* Some kind of keyboard lighting control would be great. I like the ridiculous
  rainbow, but I want it to be static, not animated.
* I want to completely disable the trackpad when a mouse (Bluetooth in my
  case) is connected.
* I should read up on YaST2 and `zypper`.
* I want to use Wayland. I have no idea how to debug the hang because I can't
  switch to a console. Hopefully an update will fix this soon, but I should
  also try to dig up some info on how to debug the SDDM to Wayland startup
  sequence.

{{< alert "bomb" >}}
Somehow this is *still* less irritating for me than just using Windows 11
day-to-day.
{{< /alert >}}

## Related Posts

* [Framework 16]({{< ref "/posts/2024/framework16" >}})
* [Why don't I just switch to Linux?]({{< ref "/posts/2022/Linux" >}})
* [Why didn't I just switch to Linux earlier?]({{< ref "/posts/2022/Linux2" >}})
* [Upgrading Kubuntu 22.10 to 23.04]({{< ref "/posts/2023/Linux" >}})

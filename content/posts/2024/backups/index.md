---
title: "Backups"
date: 2024-04-20T14:51:12-04:00
draft: false
showHero: true
tags: ["Howto", "Linux", "Tools", "Backups"]
categories: ["Linux"]
---

Please back up your files (and a how-to for `restic` on Linux)!

## Yeah, Yeah, I Know

Everyone says to back up your important files. Not everyone does. Everyone
wishes they had as soon as their computer blows up.

If you're not backing up your files, please set up *something* as soon as
you can. There are *lots* of options available, including things you can do
for free.

I'm going to write about what *I* do, and why I think this is enough for me.
You'll have to decide what works well for you, of course, but hopefully I can
provide a bit of advice.

I'll also write a bit about [`restic`](https://restic.net/) and how I use it
on my Linux laptop. `restic` *is* a command-line tool, but it works on macOS
and Windows as well as Linux.

{{< alert "comment" >}}
**TL;DR** Just go install something like [Backblaze](https://www.backblaze.com/)
on your computer.
{{< /alert >}}

## Best? Practices

To figure out the "best" way to back up your important files, you'll have to
think about possible disasters, escalating:

- Computer file system problems (it's corrupt, you've deleted something
  important, the hard drive has died). Or your computer got stolen/lost.
- Catastrophe (house fire, everything got stolen).
- Internet is down (temporary? forever?).

![Boy, that escalated quickly!](that-escalated.gif)

I like to use a layered backup regimen to (hopefully!) mitigate various levels
of disaster:

- Backups on my computer. Ideally on a separate hard drive, or at least a
  separate filesystem, but even a folder somewhere is better than nothing. An
  external hard drive is an easy (and surprisingly cheap!) way of adding a
  spot to store your backups.
- Backups on the network in my house. I've got a little two-bay NAS device
  (a
  [Synology DiskStation](https://www.synology.com/en-ca/products?product_line=ds_j%2Cds_value))
  but some wifi access points have USB drives where you can connect an external
  drive to share. You could also share things between different computers on
  your network.
- Backups in the cloud. I really like [Backblaze](https://www.backblaze.com/),
  but there are *lots* of options here. If you're comfortable looking after
  your passwords (**PROTIP:** Use a password manager of some kind.) look for
  a service or tool that lets you encrypt your data before it gets stored in
  the cloud.

The local backups (on your computer or an external drive) give you some
protection from minor disasters. If it's a separate filesystem (either a
spare partition on your hard drive, a second hard drive, or an external drive)
it'll protect you from corrupt filesystems (and even drive failures if it's
another physical piece of hardware; even *ransomware* attacks if it's an
external drive that you only connect when doing backups).

These backups should be *frequent* and *automatic* so you don't have to think
about it. I like to keep a mix of full backups (a complete snapshot of your
files) and incremental backups (changes since your last full backup).

{{< alert "comment" >}}
External drives can be taken to your office, or stored at a friend's house to
provide "cloud" type protection, but you'll have to set up some sort of
schedule for keeping them up to date.
{{< /alert >}}

The home network backups protect you from bigger computer disasters, like using
an external drive.

These can be less frequent, but should still be *automatic*.

Cloud backups protect you from huge disasters. These don't need to be stored
with a huge cloud provider, you're just looking to move things further away
from your house/office.

These backups can be the least frequent, but should still be *automatic*.

### What to Include

*Literally everything* that you can't easily replace/recreate. Everything that
you've made or collected (documents, photos, videos, music, game save files,
programming projects, *etc.*), and everything you've spent ages setting up just
the way you like it.

### What To Exclude

Things that are already stored somewhere else (think cloud drives like Google
Drive or Sync.com), things you can install easily (applications, Steam games),
large caches, etc. This list is going to be specific to you, and will depend
on your operating system and what software you've got installed.

Don't know what to exclude? *Don't worry about it.* It'll take longer and use
more storage, but at least you won't lose something important by accident.

### Is It Even Working?

The only way to make sure your backup scheme is working is to *test it*.

If you've got the disk space, try doing a restore to a new directory. No
errors? Files looking good? For added protection, you can use something to
compare all the files/folders in the restored directory with the originals
(but remember how old your backup's files are compared to what's currently
on your computer).

If your backup tool supports it, you can do a test that reads through all the
data and checks its validity (good backup tools store your data, but also a
checksum to make sure the data doesn't get corrupted). This gives you confidence
that the *backup data* is in good shape for when you need it, and is your only
option if you haven't got spare disk space to do a restore.

## Using `restic`

[`restic`](https://restic.net/) is a really nice command-line backup tool that
supports a *lot* of destinations (local, network, cloud). I've got a `git`
repo on Codeberg for my
[`restic` scripts](https://codeberg.org/Taffer/restic-scripts/) and the
`systemd` unit files I've been using on Linux.

{{< alert "comment" >}}
`restic` supports Windows and macOS, too (and can probably be built on "any"
system that supports the Go programming language).
{{< /alert >}}

### Local Backups

*Every hour*, my system does a backup of my home directory (minus the *various
things* I exclude from my backups) to a separate partition. Ideally this would
be a second hard drive, but not all laptops let you add a second one, etc.

{{< alert "comment" >}}
I keep *everything* important in my home directory so I can easily keep track
of everything.
{{< /alert >}}

I keep 7 daily backups (so per-day snapshots), 4 weekly backups, and 3 monthly
backups. I let `restic` manage the "full" vs. "incremental" backups rather than
explicitly doing full backups from nothing. I have no idea if 7/4/3 is a good
set of backups, or overkill, or what, it just seemed reasonable.

When this runs, `restic` also checks the backup repo for errors and cleans up
any old backup data.

You can see how in
[this shell script](https://codeberg.org/Taffer/restic-scripts/src/branch/main/bin/backup-local.sh)
(if you can read shell scripts; if you can't, give it a try anyway, it might
still make sense).

### Home Network Backups

*Every day*, my system does a backup of my home directory (minus the excluded
files) to my network storage.

I keep 7 daily, 4 weekly, and 3 monthly backups, like I do with the local
backups.

Again, `restic` checks the backup repo and cleans up the old backup data.

You can see how in
[this shell script](https://codeberg.org/Taffer/restic-scripts/src/branch/main/bin/backup-nas.sh).

### Cloud Backups

*Every week*, my system does a backup of my home directory (minus the
excluded files, of course) to Backblaze's B2 cloud storage. I really like
Backblaze's backup service, but it only supports Windows and macOS; B2 has a
great price for storing lots of data, and doesn't charge an extra fee for
downloading your data, which is vital for testing and restoring.

I keep 4 weekly, and 3 monthly backups.

Naturally `restic` checks the backup repo and cleans up the old backup data.

You can see how in
[this shell script](https://codeberg.org/Taffer/restic-scripts/src/branch/main/bin/backup-b2.sh).

### Extras

I've got a
[script](https://codeberg.org/Taffer/restic-scripts/src/branch/main/bin/backup-show-snapshots.sh)
to show a quick list of the backup snapshots in each repo.

I've also got a
[script](https://codeberg.org/Taffer/restic-scripts/src/branch/main/bin/backup-test-archives.sh)
that does a full test of each repo; this can take a while (especially for the
cloud repo) as it downloads and tests *all* of the data.

Every week I also run a separate backup of my music collection and some archive
data to B2. I've broken these out into separate scripts because the data doesn't
change very often, and the data sets are *large*… I wanted to make sure these
went to separate repos so I could tweak the frequency and backup sets:

- [Music script](https://codeberg.org/Taffer/restic-scripts/src/branch/main/bin/backup-b2-music.sh)
- [Archives script](https://codeberg.org/Taffer/restic-scripts/src/branch/main/bin/backup-b2-archives.sh)

[Here](https://codeberg.org/Taffer/restic-scripts/src/branch/main/systemd) you
can see the `systemd` service and timer units that I use to automate the
backups, ensuring I don't forget to do them regularly. The `.timer` units
control when the `.service` unit gets activated, which runs the appropriate
backup script.

{{< alert "comment" >}}
There's probably a more clever way of setting this up. I don't really care,
because this was simple and is working, and I haven't needed to make anything
more complex.
{{< /alert >}}

---
title: "Framework 16"
date: 2024-03-21T17:00:08-04:00
draft: false
showHero: true
tags: ["Howto", "Linux", "Upgrades", "Framework"]
categories: ["Linux"]
---

In July last year, I eagerly pre-ordered a Framework 16 laptop. It finally came,
so here's my experience "building" it and some comments about my first-week
impressions of the hardware.

Since you want to see the result:

![Fully assembled Framework 16 running OpenSUSE Tumbleweed](fw15.jpg)

## Waiting

I knew it was going to be a while before the
[Framework 16](https://frame.work/ca/en/products/laptop16-diy-amd-7040)
hardware actually started shipping. Hardware pre-orders always seem to
take *forever*, and I've definitely never done one this big…

My previous laptop is an XPG Xenia 15 (Core i7, 32GB RAM, 1TB SSD and an
aftermarket 2TB SSD) which I've been really happy with. It's a "gamer"
laptop, so it's got an RGB keyboard, a discrete nVidia GPU, and a massive
power adaptor. It's been great in Kubuntu, except for flakey wifi (a
Linux classic!). I did have to send it for replacement at one point due to
a partial logic board failure (*possibly* caused by knocking it off a foot
stool), but AData's North American support team were great; they even put
my secondary 2TB SSD into the new machine before sending it back.

I wasn't in a huge rush to replace the Xenia 15, but had an opportunity:

* I *love* what Framework are doing with repairable/upgradeable hardware.
* I really wanted to try out an AMD-based system. I've been on Intel/nVidia
  laptops since the original "Unibody" x86 Macs shipped.

## Go home FedEx, you're drunk

It was only a week-ish between getting the "We're readying your batch!" email
and the "It's shipped!" email, which was awesome. FedEx, on the other hand,
did something weird:

* Package travelled from Taiwan to Alaska, to Manitoba. So far, so good, it's
  going to come on Friday and I can set it up over the weekend!
* No, wait, it's gone to Memphis for some reason?!
* No updates for 3-4 days.
* Oh, hey, *back to Manitoba*, then Mississauga, then to me.

![Molly posing with her Yeti beside the somewhat beat up shipping box](featured.jpg)

## Build it

Following the
[Framework Laptop 16 DIY Edition Quick Start Guide](https://guides.frame.work/Guide/Framework+Laptop+16+DIY+Edition+Quick+Start+Guide/270)
was easy.

### Unpacking

All of the Framework 16 packaging is recyclable/compostable paper, except one
plastic piece that's also recyclable. This big envelope had two sticker
sheets (awwwww yissss), an iron-on patch (what am I going to do with this?),
a picture sheet of art like one of the sticker sheets, and a small warrantee
type of pamphlet with extremely tiny text.

![Unpacking, there's a big envelope with stickers, an iron-on patch, etc.](fw01.jpg)

All the main parts, plus the free screwdriver. Not pictured are the six
expansion cards. My order:

* Ryzen 7 7840HS
* no memory, no storage, no OS
* Expansion bay: Graphics Module (AMD Radeon RX 7700S)
* RGB English keyboard
* two keyboard spacer modules; I don't want a numeric keypad, but I *really*
  want a centred keyboard
* Black bezel
* North American power adaptor
* Expansion cards: 2x USB-C, 2x USB-A, HDMI, and audio jack

I didn't order the Ethernet expansion card because it sticks out (ew), and
I've got a USB-C dongle with Ethernet for those rare times when I'm at a
desk. I mostly use my laptop on my lap…

![Unpacked, except for the port socket thingies.](fw02.jpg)

All the parts unpacked, including the expansion cards. The bezel weighs
nothing and feels like it might be really fragile until you add it to the
screen.

Also pictured are a pile of way too many stickers that I'm going to have to go
through, a lovely Steam Deck, and a full-spectrum light for those weeks-long
gloom sessions we get during winter.

![Ready to start! The Framework 16 parts, plus some RAM and an M.2 SSD. And some other stuff on my desk.](fw03.jpg)

### Building

When you open the laptop to get started, you find the big plastic piece. I was
mildly disappointed to find this, until I saw the various recycling logos on
it… the plastic is PETE (or PET?), like pop bottles. This protects the screen
from the metal shielding during shipping.

![When you open it, there's a big plastic insert to protect the screen from the internals.](fw04.jpg)

Hey, remember when computers were built by cool people who hid their names
inside the case, or on the mainboards? Framework clearly does! I love that
the Framework team gets credit for their much-appreciated work.

![The Framework team gets credit! A list of the folks who worked on the laptop.](fw05.jpg)

Some more credits on the other side, plus a link to Framework's careers
site [jobs.frame.work](https://jobs.frame.work/).

![More credits! And a link to Framework's job site.](fw06.jpg)

One tiny plug thing (the orange bit) that pulls straight up (and easily plugs
back in), plus sixteen tiny screws. The tiny screws are captive, which is a
brilliant idea for a system that's meant to be taken apart. The included
screw driver fits them perfectly, of course, and is magnetic.

![Ready to take off the metal shield that goes between the keyboard and mainboard.](fw07.jpg)

Underneath, you can see all the goodies; I love a laptop with four slots:

* 2x RAM (DDR5 5600)
* 1x M.2 2280 (the normal laptop SSD form factor)
* 1x M.2 2230 (the tiny kind, like in the Steam Deck and a few other things)

![The internals are nicely laid out, with two open slots for RAM, one M.2 2230 SSD, and one M.2 2280 SSD.](fw08.jpg)

An XPG Gammix S70 Blade (*sigh*) SSD slotted into the M.2 2280 slot. I was
happy with my XPG laptop and the extra XPG SSD I added to it, so why not buy
the current model.

4TB (so much room for activities!) is cheaper than getting 2TB plus a 2TB
M.2 2230 (those are still quite expensive), which was my original plan, since
I like having separate physical disks when I can for backups.

![XPG Gammix S70 Blade M.2 2280 SSD with some blur, unfortunately.](fw09.jpg)

One of the sticks of RAM installed. At this point I put everything back
together and verified that I could boot the system; I'd heard they might be
problems booting it with two sticks initially. :shrug:

![One stick of RAM installed.](fw10.jpg)

All of the tiny screws were captive, *except this one*. When I was taking the
metal shield off it apparently came out, but it just stick to the little
magnet just to the right of it in this pic.

Good thing, or I would've lost it somewhere. They're *tiny*.

![One cursed screw.](fw11.jpg)

The final product, with all of its RAM, storage, and expansion cards
installed. Because of the CPU chipset's limitations, the back four expansion
bays have slightly different characteristics than the front two. I've got my
expansion cards installed like this (when viewed from the top, front to back):

|Left Side|Right Side|
|---------|----------|
|USB-C    |USB-C     |
|HDMI     |USB-A     |
|USB-A    |Audio     |

The USB-C ports can be used for power and driving external displays.

![All together and ready to boot something, in this case memtest86+.](fw13.jpg)

## It's alive

Here's a shot of it, successfully booted and in the middle of installing
[OpenSUSE Tumbleweed](https://www.opensuse.org/#Tumbleweed). I think this is
the partition editor.

I'm going to write another post about my early experiences using Tumbleweed on
the Framework 16.

![Installing OpenSUSE Tumbleweed; I think this is the disk partition editor.](fw14.jpg)

Yay, it's done! OpenSUSE is installed and booted, and ready for me to mess up.
I spent the evening installing most of my essential software.

![Installed and booted!](fw15.jpg)

## Comparison with XPG Xenia 15

To see the difference in size between the XPG Xenia 15 (a fairly typical 15"
laptop), I put it on top of the Framework 16. As you can see here, they're
roughly the same thickness.

![Framework 16 with the XPG Xenia 15 on top, side view; they're about the same thickness.](fw16.jpg)

Here's the top view. The perspective's a bit weird here, they're basically the
same width. The Framework 16 is deeper (16:10 display instead of 16:9), plus
the expansion bay sticks out a couple of cm on the back.

![Framework 16 with the XPG Xenia 15 on top, top view; same width, Framework 16 is deeper.](fw17.jpg)

## Hardware very-mini review

Overall, this seems like some great hardware, and even if I don't end up
upgrading parts of it in the future (I probably will…) I love the prospect of
being able to replace pieces if/when they get damaged, without having to
throw out the entire thing.

The screen is great and bright, but I'm still adjusting to having more space
and a higher resolution. I haven't noticed any dead/stuck pixels, and the
colour seems great.

The RGB keyboard defaults to gamer mode: a rainbow that cycles from left to
right. All the time. I don't have software yet to control this, which is
self-inflicted because I insist on using Linux. Typing on it is comfortable,
although I wish the arrow keys were larger somehow.

Some nit-picks:

* The spacer panels, which are absolutely required for the flexibility
  around keyboard/trackpad layouts, don't perfectly line up with the edges of
  whatever they're next to. This is mildly annoying from an aesthetic point
  of view, but I don't know if it can be fixed without losing the
  flexibility.
* The front edges of the trackpad module, where my wrists sort of rest while
  I'm typing, feels sort of sharp, and might physically irritate me during long
  sessions. I'm sort of hoping for a replacement set of trackpad module and
  spacers with a more rounded edge.

Overall, I'm quite happy with the Framework 16, even though buying the first
generation of something is frequently painful. Great work, Framework team!

## Related Posts

* [Why don't I just switch to Linux?]({{< ref "/posts/2022/Linux" >}})
* [Why didn't I just switch to Linux earlier?]({{< ref "/posts/2022/Linux2" >}})
* [Upgrading Kubuntu 22.10 to 23.04]({{< ref "/posts/2023/Linux" >}})
* [Wifi Horrors]({{< ref "/posts/2023/Wifi" >}})
* [Wifi… Optimism?]({{< ref "/posts/2023/Wifi2" >}})

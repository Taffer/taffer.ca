---
title: "Internal Tools"
date: 2024-09-18T09:22:17-04:00
draft: false
showHero: true
tags: ["Enshittification"]
categories: ["Work"]
---

An exciting new opportunity!

We've been working hard for years now to ensure that we're sacrificing
usability in favour of maximizing profit at all costs, but there's a
demographic that remains under-monetized.

Your employees.

It's bad enough that we have to pay them (just wait for AI to be useful!), but
we're leaving money on the table here.

Most companies are already making employees pay for the privilege of working
there, through un-reimbursed transportation costs, time lost to commuting, etc.
Some are even smart enough to own the parking that employees have to pay for
at the offices they're being forced to attend (brilliant!).

But are you advertising to them?

Just think:

- You have all kinds of internal tools that employees are forced to use all day,
  every day.
- You can disable ad blockers via IT policy.

This makes 100% of your staff (minus the executives, of course) a captive
audience for advertisers! 100% of all impressions will be directly to
*real humans!*

And just think of all that demographic data you can synthesize from HR systems,
job descriptions, daily tasks, etc. Verified data is *extremely* important to
advertisers!

You can even make clicking on a certain number of ads every day part of their
job requirements.

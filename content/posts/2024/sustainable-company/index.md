---
title: "Sustainable Company"
date: 2024-02-10T07:07:14-05:00
draft: false
tags: ["Remote", "WFH", "Sustainability"]
categories: ["Work"]
---

Some thoughts on how I'd set up and run a company, if only I had the resources
to do so.

The idea here is to create a stable, sustainable company whose goal isn't to
make rich people richer. Crazy, right? No "moon shot" where we bet the entire
company on one project championed by the loudest person in the room. No cash-out
IPO where the founders load up on cash and everyone else is left working for a
living until the shareholders or buyers lay them off for short-term gains.

I should probably note that I'm not a "business" person. My areas of expertise
are computers, security, cryptography, and communication. But I read a lot, and
my brain chews on problems whether I want it to or not.

This will probably sound like a
[communist](https://en.wikipedia.org/wiki/Communism) or
[socialist](https://en.wikipedia.org/wiki/Socialism) manifesto. Please look
those terms up some time if you use them to dismiss ideas. And I'm definitely
a socialist.

## A Sustainable Company

This company exists to improve the lives of the employees, and by extension the
communities that it operates in. The goal is to have enough income to pay the
employees, give back to the community, and (presumably) pay back the investor(s)
so it becomes completely independent.

By "community" I mean both the physical communities where the company and
employees live, and the wider virtual community that the company serves.

Supporting employees:

- compensation; above "market", and guaranteed yearly cost-of-living increases
  (the previous year's inflation rate +1%)
- profit sharing
- continuous learning: books, courses, conferences, etc. for literally anything
  the employee is interested in, even if it's not directly "work-related"
- unlimited PTO; you want a 4-day work week? book off a day every week
- be *sustainable* so it can survive through lean times without firing people
- extensive coaching for people who aren't thriving
- strategic growth so the company doesn't become over-extended
- advice from investors, not interference
- the ratio between highest salary and lowest salary will be single digit
- no sales quotas or commissions
- virtual office; no emissions from commuting, no money wasted on commercial
  real estate
- frequent virtual get-togethers, occasional physical get-togethers (maybe
  quarterly if/when we're safe from spreading potentially debilitating
  respiratory diseases)

Supporting communities:

- paying the company's fair share of taxes; no tax dodging schemes, no lobbying
  for tax discounts, etc.
- paid volunteering time (I said unlimited PTO)
- company-matched donations to charities, non-profits, open source
- company matching to retirement savings (RRSP in Canada, 401(k) in the USA)
- actually paying for the open source software the company depends on
- contributing back to the open source projects the company depends on

## Ideas

I've got ideas that could lend themselves to this concept:

* software development and resource management tools for video game companies
  and media companies (they have a shared set of pain points)
* "IoT that doesn't suck" - open IoT devices and server using open protocols
  and not requiring cloud services (so the devices don't become e-waste when
  the cloud company focuses on something else); host your own server locally
  or at a hosting provider (or we'll have a cloud that's reasonably priced)

I've got more written down, these are just the two that have been rattling
around in my head lately.

With the herd-mentality (or *orchestrated*) layoffs in tech and video games
this year, now's a fantastic time to be hiring people and starting new
projects, aiming for 2-3 years down the road when things have swung back
towards companies investing in new things instead of rent seeking.

## The Problem

The problem is that I don't have the resources to start a company on these
principals, and the wealth hoarders who do have no interest in helping anyone
unless it directly benefits them.

If you're rich and somehow still altruistic, or at least have an interest in
video games or using your wealth to make things better, please contact me.
Until then, I'll keep buying lottery tickets (I think of it as a weekly tax
that supports good things like libraries and hospitals).

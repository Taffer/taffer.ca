---
title: "Auto-deploy: Hugo and Codeberg CI"
date: 2024-04-27T05:36:25-04:00
draft: false
showHero: true
tags: ["Hugo", "Codeberg", "Git", "Howto", "Tools"]
categories: ["Linux", "Website"]
---

A description of how I've set up Codeberg's CI to automatically build and
deploy changes to my website.

Woodpecker image courtesy of
[Wikipedia](https://commons.wikimedia.org/wiki/File:FemaleHairywoodpecker.jpg).

## Dramatis Personae

The infrastructure:

* [Codeberg](https://codeberg.org/), "a democratic community-driven, non-profit
  software development platform operated by Codeberg e.V. and centered around
  Codeberg.org, a Forgejo-based software forge."
* [Dreamhost](https://www.dreamhost.com/) is where I host
  [my website](https://taffer.ca/).

The tools:

* Codeberg's [Woodpecker CI](https://woodpecker-ci.org/) instance at
  [https://ci.codeberg.org/](https://ci.codeberg.org/).
* [Hugo](https://gohugo.io/), "the world’s fastest framework for building
  websites."
* [`rsync`](https://wiki.archlinux.org/title/rsync), a fast incremental file
  transfer tool.

## How It Works

In mid-January this year, I moved my website from Dreamhost's WordPress
setup to Hugo, a static site generator. This removed a *lot* of attack
surface (no PHP, no database, no WordPress, and the only JavaScript is for
switching between Light and Dark modes) and let me write pages using
Markdown and my favourite text editor.

The process is really simple:

* make changes, viewing them locally with Hugo's server mode
* check the changes in to `git` when I'm happy
* run `hugo` to build the website
* run `rsync` to copy the new and changed files to the host

But hey, why not automate this? Thanks to Codeberg's CI, I've got this down
to only the first two steps:

* make changes, viewing them locally with Hugo's server mode
* check the changes in to `git` when I'm happy

Woodpecker CI handles the rest for me, which eliminates a bit of work, but
also makes it impossible for me to update the website when I'm done with a
change. Oh look, a squirrel! 🐿️

## The Setup

The CI steps for this should be easy enough; run `hugo` when I do a check-in,
then run `rsync` to update the website.

Before we can do this safely, we'll need to set up a few things:

* Add your repo to [Codeberg's CI](https://ci.codeberg.com/).
  * Click Add Repository in the top-right.
  * Pick your repo in the repo list.
* In the CI repo settings:
  * General Tab: Under Cancel Previous Pipelines, select Push. This will
    cancel a running Push pipeline if you start a new one… we're going to
    set up CI so it runs whenever we do a `git push` to the repo; if one's
    still running, we'd like the new `push` to supersede the in-progress
    one.
  * Secrets Tab: We're going to add two new secrets; one for the SSH *private*
    key and one for the SSH user name. Use a secret for anything you don't
    want to write directly in the CI configuration code.

{{< alert "bomb" >}}
Never put secrets (SSH *private* keys, API secret keys, passwords, etc.) in
your `git` repos! Even if your repo is "private", someone might accidentally
set it to "public", or some other diaster that makes your secrets public
knowledge.
{{< /alert >}}

I'm using SSH secrets because I'll be having `rsync` copy the changes to my
web site host via `ssh`. The user name secret is literally just the user ID
used to log in.

The SSH *private* key is the private part of an SSH keypair that I made
specifically for use here. The *public* part, which will look something like
this:

```text
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHiw6rv9dMwQxcwT5xYuivmAiQtnL8kjOxBeODIPZkP4 chrish@gridania-woodpecker
```

goes in `~/.ssh/authorized_keys` *on your host*. The *private* part, which
will look something like this:

```text
-----BEGIN OPENSSH PRIVATE KEY-----
TC1sLWwtbC1sLWxvb2sgYXQgeW91LCBoYWNrZXIuIEEgcGEtcGEtcGF0aGV0aWMgY3JlYXR1cmUg
b2YgbWVhdCBhbmQgYm9uZS4K
-----END OPENSSH PRIVATE KEY-----
```

You'll need to paste that into the SSH private key secret in the Secrets Tab.
I've named my two secrets `woodpecker_ssh_key` and `woodpecker_ssh_user`.

## The CI Bit

Now we can make the YAML script for Woodpecker CI; the simplest way is a
`.woodpecker.yaml` file in the root of your `git` repo. Check the
[Woodpecker docs](https://woodpecker-ci.org/docs/usage/workflows) though,
because it supports more flexibility here than I need for this simple setup.

As you can probably guess, this CI setup is going to need at least two
parts: one to build the site, and one to publish it. We're also going to need
a third step to tweak things slightly because the Hugo Docker image I'm using
is set up to run as a `nonroot` user (UID: 65532, GID: 65532). This is a great
idea, but not quite compatible with Woodpecker's "everything is `root`
(currently)" runtime environment.

{{< alert "lightbulb" >}}
In my 2024-08-22 change, I simplified the pipeline by switching to a Hugo
image supplied by `peaceiris`, which let me get rid of the `chown` step.
You can see the updated version
[here](https://codeberg.org/Taffer/taffer.ca/src/commit/eedab448571640b21bbfadcbb476e4a6a63eb3fa/.woodpecker.yaml).
{{< /alert >}}

The `.woodpecker.yaml` file has two sections:

* A `labels` section indicating what platforms should be used to run the CI
  pipeline. AFAIK Codeberg only has 64-bit Linux infrastructure right now, which
  is fine. There's nothing in here that's CPU-specific though, we could easily
  run on a 64-bit ARM platform.
* The `steps` section, which lays out the pipeline's steps. Each step is run
  after anything it `depends_on`; with more complex pipelines, it'll attempt
  to parallelize steps when it can. This pipeline's steps all run on a `push`
  event, which is whenever I push changes to the repo.

{{< alert "comment" >}}
The `codeberg.org/taffer/ubuntu-rsync-ssh` Docker image used to run the `fix-permissions`
and `publish` steps is literally just the latest Ubuntu base image with
`rsync` and `ssh` installed. I made a custom image because I couldn't find
one that already had both `rsync` and `ssh`. 🤷
{{< /alert >}}

```yaml
# Build website via Hugo, push to server.
---

labels:
  platform: linux/amd64

steps:
  # Chainguard's Hugo image runs as nonroot 65532:65532, so we have to do
  # this to let it actually write files properly.
  #
  # Woodpecker's sandbox is making everything under root:
  # https://github.com/woodpecker-ci/woodpecker/issues/1077
  - name: fix-permissions
    image: codeberg.org/taffer/ubuntu-rsync-ssh:latest
    when:
      - event: push
    commands:
      - chown -R 65532:65532 .

  - name: build
    image: chainguard/hugo:latest
    when:
      - event: push
    depends_on:
      - fix-permissions

  - name: publish
    image: codeberg.org/taffer/ubuntu-rsync-ssh:latest
    when:
      - event: push
    secrets: [woodpecker_ssh_key, woodpecker_ssh_user]
    depends_on:
      - build
    commands:
      - mkdir ~/.ssh
      - chmod 700 ~/.ssh
      - printf '%s\n' "$${WOODPECKER_SSH_KEY}" > ~/.ssh/deploy_key
      - printf "Host pdx1-shared-a1-35.dreamhost.com\n\tIdentityFile ~/.ssh/deploy_key\n\tUser $${WOODPECKER_SSH_USER}\n\tStrictHostKeyChecking=no\n" > ~/.ssh/config
      - chmod 600 ~/.ssh/*
      - rsync -azv --delete public/ pdx1-shared-a1-35.dreamhost.com:taffer.ca/
```

There's a bunch going on in the `publish` step. The `secrets` section gives
this pipeline step access to the named secrets; they get added to the step's
environment in *upper-case* (so `WOODPECKER_SSH_KEY` for the secret named
`woodpecker_ssh_key`).

The first five lines of the `commands` section create a `~/.ssh` directory
with the right permissions, then sets up the SSH *private* key from our
secrets, and an SSH `config` file so we can connect to my web host using that
private key, the right user ID, and with `StrictHostKeyChecking` turned off.

{{< alert "bomb" >}}
I'm still not exactly sure whether `StrictHostKeyChecking=no` is required
because of the way Dreamhost is configured, because of how Codeberg's
Woodpecker CI instance is configured, or if I've just missed a package in the
`codeberg.org/taffer/ubuntu-rsync-ssh` Docker image.
{{< /alert >}}

Finally, `rsync` does its magic, copying new and changed files, and deleting
removed files from Hugo's build directory (`public/` in the CI sandbox) to
the website directory on the web host.

Add that to your repo, check it in, and push it, and watch Codeberg's CI update
and publish your website automatically!

---
title: "Vivaldi"
date: 2024-07-28T18:15:35-04:00
draft: false
showHero: true
tags: ["Tools"]
categories: ["Tools"]
---

I used Vivaldi as my web browser (and email client, and RSS feed reader) for
a week or so, and these are my impressions/notes/complaints.

I've used Firefox *forever*, with brief periods of using Chrome and Brave.
Chrome never felt "right" somehow (some weird aspect of the UI that I've
never figured out), and Brave is just Chrome with crypto nonsense inside.

I'm mildly concerned with Mozilla getting generative AI brainworms, and their
executives seem to make amazingly stupid decisions every few months. Why not
check out an inoffensive Chrome flavour?

{{< alert "comment" >}}
The browser screen shot image is from [Vivaldi's website](https://vivaldi.com),
I hope they don't mind.
{{< /alert >}}

## Vivaldi

[Vivaldi](https://vivaldi.com)'s a browser based on the Chrome code, with a few
differences:

* No Google spyware (this is the big one)
* No Vivaldi spyware
* Various UI tweaks, especially around tab management and customization
* Built-in ad/tracker blocking
* Built-in email, calendar, contacts, feed reader, notes
* Not run by a gigantic advertising company

It works with standard Chrome extensions, and is extremely fast.

## Thoughts

In no particular order, here are some thoughts and observations I've had while
using it for my normal work and web browsing.

* Very fast, haven't felt the UI lag anywhere.
* Built-in ad blockers seem to work well; I can do things on YouTube without
  being interrupted constantly (so much better than trying to watch a YT video
  on my TV).
* Found a banner ad that wasn't blocked (DailyWTF, some Artifactory "survey"
  banner, probably a static image hosted directly on DailyWTF), and some kind
  of text ad at the bottom of the article; installed Privacy Badger (no effect),
  and uBlock Origin Lite (victory!).
* After adding the ad blockers, it seems to give a ~15s pause (or longer) before
  YT videos start; I assume this a pre-roll ads? Punishment for having uBlock
  installed? Never had this happen in Firefox… this is inconsistent, so maybe it
  was just something in the video I watched? Others videos don't have it. If
  this is YouTube's ad-blocker punishment in action, it really just makes it
  look like the site is broken.
* YouTube yelled at me for using ad blockers today, so that's not good.
* Warming up to Workspaces, I've stuffed all my open Godot-related tabs into one
  to tidy up, and I've got another full of resources for learning Rust.
* Opening a folder of tabs is backwards, it puts focus on the last tab; I open
  1-2-3-4, 4 has focus, and it's treated as a stack of 4-3-2-1… if I switch to
  1 and close it, the tab I was using before opening them gets focus again. For
  what I usually use this for (daily news sites) I should probably use a
  Workspace. This is one of those weird little UI niggles where something isn't
  *quite* what I expect, and the behaviour isn't actually obvious. I want tabs
  to work like Firefox, of course.
* The 1Password plugin doesn't use the OS authentication window, so I have to
  type my password when it times out instead of using my fingerprint; this might
  be a Chrome problem, I don't know as I don't use Chrome. Platform is Linux,
  but Firefox does this correctly.
* Vivaldi remembers its size/position, but starts hidden every time, I have no
  idea why.
* Ah! It's remembering that it was hidden when it closed (generally a reboot to
  pick up a new kernel), which was unexpected.
* Default behaviour? or something I've set up accidentally? typing in the URL
  bar auto-completes full URLs, not domains, so I'm constantly pulling up old
  web pages.

So generally fine, the Workspaces feature seems pretty great, and there are
many (*many*) customization options in the settings. On the down-side, these
options could *really* use tooltips or something to explain them. I'm sure
their names are 100% clear to the developers, but a newbie coming from other
browsers might be lost as to their purpose.

### Email

For some reason, even thought I'd set Vivaldi as the default browser in my
OS' default apps preferences, Thunderbird kept firing up everything in
Firefox. So I tried out Vivaldi's email client, too.

**Update:** I switched back to Firefox/Thunderbird, set the default browser in
KDE's prefs, *and* let Firefox set itself as default browser. Links keep
opening in Vivaldi, so this appears to be a KDE problem.

* Email's "external content" controls are very coarse compared to Thunderbird; I
  try to allow images, but not trackers... maybe the built-in ad blockers handle
  that?
* No CardDAV support for contacts.
* Adjusting to new UIs is always a bit annoying 😉; *think* I prefer stand-alone
  mail, but that could just be the UI being awkward or unfamiliar?
* Can't figure out how/where to install a Canadian English spelling dictionary;
  ah, apparently I had to click in a text field, and change it through a context
  menu. 🤯 <https://help.vivaldi.com/desktop/tools/spell-checker/>
* After 5 days or so, Vivaldi asked to be my default browser, and now
  Thunderbird opens links there… so the bug is in KDE's "default browser"
  setting? Or there's more than one default browser setting?
* I like having email in an external app, but Thunderbird currently being unable
  to remember its size/position is surprisingly annoying.

The current weird Firefox/Thunderbird UI bug (the windows absolutely cannot
remember where they belong, or what size they should be, every launch brings
them up in the default position at the default size) is one of the reasons why
I was looking around for non-Chrome browsers.

### RSS

* The built-in RSS is only useful if you've got a very small number of feeds,
  it's extremely bare-bones… need to use FeedBro (a Chrome extension) or
  something.
* FeedBro works pretty well, it's able to *reliably* poll my feeds, and load
  feeds that Thunderbird fails on (Thunderbird doesn't tell me why it's not
  pulling some feeds, it just doesn't get any content).

## Summary

If you currently use Chrome, Edge, Brave, or any of the other Chrome-based
web browsers, you should switch to Vivaldi. It's the best Chrome experience
I've had.

I'll probably go back to Firefox, and keep praying to Cthulhu that Mozilla
doesn't do anything stupid enough to make Firefox and Thunderbird unworkable.

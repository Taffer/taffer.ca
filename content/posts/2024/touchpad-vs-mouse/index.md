---
title: "Touchpad vs Mouse - KDE on Wayland"
date: 2024-04-12T17:36:28-04:00
draft: false
showHero: true
tags: ["Linux", "OpenSUSE", "Howto"]
categories: ["Linux"]
---

I figured out a way to automatically enable or disable my laptop touchpad
when my mouse is disconnected or connected!

## Background

I've installed [OpenSUSE Tumbleweed](https://en.opensuse.org/Portal:Tumbleweed)
on my laptop. This uses KDE Plasma 6 running on Wayland for the GUI.

I *always* use a mouse with my laptops. The touchpads are usually terrible
(except Apple hardware, they've really got the software nailed down), and I
expect things to work properly.

When I type, my palms or whatever seem to frequently touch the touchpad, causing
clicks and moving the mouse all over. This drives me *insane*.

Most OSes have an option to disable the touchpad when you connect a mouse.
KDE does not, it's just got an option to disable it while typing… this doesn't
seem to work properly, ever.

## Solution

This took me *weeks* to figure out, so hopefully this saves you some effort.

First, we create some
[`udev` rules](https://wiki.archlinux.org/title/udev#About_udev_rules) in
`/etc/udev/rules.d/99-bt-mouse.rules`:

```ini
# Logitech BT mouse
SUBSYSTEMS=="input", ATTRS{name}=="MX Anywhere 3S Mouse", ACTION=="remove", RUN+="/bin/su chrish -c /home/chrish/bin/mouse-off.sh"
SUBSYSTEMS=="input", ATTRS{name}=="MX Anywhere 3S Mouse", ACTION=="add", RUN+="/bin/su chrish -c /home/chrish/bin/mouse-on.sh"
```

{{< alert "bomb" >}}
Clearly this is specific to the Logitech "MX Anywhere 3S" mouse that I use;
we'll go into figuring out the device name later.
{{< /alert >}}

After creating (or editing) `udev` rules, tell it to reload them:

```sh
sudo udevadm control --reload-rules
```

The rules match events, so whenever the "input" subsystem sees a device
matching the specified "name" attribute being removed or added, it's going
to run the command. By default, they run as `root`, but I want them to run
as my account so I can prod the KDE session properly.

Next, we create the scripts that are going to be triggered.

For `mouse-off.sh`:

```sh
#!/bin/sh
#
# MOUSE IS GOING OFFLINE

export DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus
/usr/bin/qdbus6 org.kde.KWin /component/kcm_touchpad invokeShortcut 'Enable Touchpad' default
```

And `mouse-on.sh`:

```sh
#!/bin/sh
#
# MOUSE IS COMING ONLINE

export DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus
/usr/bin/qdbus6 org.kde.KWin /component/kcm_touchpad invokeShortcut 'Disable Touchpad' default
```

The scripts are executable (`chmod +x`) and owned by me.

We define `DBUS_SESSION_BUS_ADDRESS` because scripts triggered from `udev`
have an almost-empty environment. Also, `udev` is a much lower-level system
and has no idea about windowing environments or desktops.

The `qdbus6` commands call the `invokeShortcut()` method of the `kcm_touchpad`
component in `org.kde.KWin` (the KDE window manager process).

If you've already had `udev` reload its rules, you can test this right away
by turning your mouse on or off.

Victory!

## Finding Your Touchpad

I had to install the `libinput-tools` package to find the name of my touchpad;
use the `libinput` command to see the name in the `Device:` field:

```sh
$ libinput list-devices | less
...
Device:           PIXA3854:00 093A:0274 Touchpad
Kernel:           /dev/input/event2
Group:            5
Seat:             seat0, default
Size:             124x77mm
Capabilities:     pointer gesture
Tap-to-click:     disabled
Tap-and-drag:     enabled
Tap drag lock:    disabled
Left-handed:      disabled
Nat.scrolling:    disabled
Middle emulation: disabled
Calibration:      n/a
Scroll methods:   *two-finger edge
Click methods:    *button-areas clickfinger
Disable-w-typing: enabled
Disable-w-trackpointing: enabled
Accel profiles:   flat *adaptive custom
Rotation:         n/a
...
```

(Not sure if it's my setup, or any touchpad setup, but there's also a
`PIXA3854:00 093A:0274 Mouse` device, presumably for mouse emulation.)

You can also get it from `~/.config/kcminputrc`, mine has a section like this:

```ini
[Libinput][2362][628][PIXA3854:00 093A:0274 Touchpad]
Enabled=false
MiddleButtonEmulation=true
```

In here, the name is that last bit of the `[Libinput]` line.

## Finding Your Mouse

If you're using a regular USB mouse, you can probably find its name via
`lsusb` or something. Mine's Bluetooth, so I have to dig a little deeper.

```sh
$ bluetoothctl devices
Device D1:FF:AB:5D:02:5E MX Anywhere 3S
```

Unfortunately, that's not quite the full name, as known by `udev`. Instead we
use `udevadm` to monitor events while we turn the mouse on and off. Note that
this will produce a bunch of message blocks for both of "on" and "off", and
we're looking for one with `SUBSYSTEM=input`:

```sh
$ udevadm monitor --property --udev
monitor will print the received events for:
UDEV - the event which udev sends out after rule processing
...
UDEV  [10133.424383] remove   /devices/virtual/misc/uhid/0005:046D:B037.0008/input/input25 (input)
ACTION=remove
DEVPATH=/devices/virtual/misc/uhid/0005:046D:B037.0008/input/input25
SUBSYSTEM=input
PRODUCT=5/46d/b037/3
NAME="MX Anywhere 3S Mouse"
...
```

And there we have it, the `NAME=` line tells us what to put in the `udev`
rules file.

## Finding the DBus Target

Figuring out what DBus message to send, and where to send it, was trial and
error, and also searching the KDE code. I used an app that connects to the
DBus session (`qdbusviewer6`) and lets you poke around at all the things that
exist. I'd found out that `KWin` (the KDE window manager) has a DBus named
`org.kde.KWin` and there was an interesting `/component/kcm_touchpad` object.

I couldn't find a way to enable/disable the touchpad directly, and I'm still
not sure if there is one.  The touchpad in KDE has three global shortcuts
though ("Enable Touchpad", "Disable Touchpad", and "Toggle Touchpad") though,
and I found the `invokeShortcut` method.

I looked in the code and found out that the setting matches the English name,
and I found that "default" is the interface for the only touchpad I have
connected.

{{< alert "bomb" >}}
I didn't use one script with "Toggle Touchpad" because the `udev` rules I'm
triggering don't seem to always have a 1:1 relationship. It's possible I
could tighten them up so they definitely only trigger once, and I could also
modify the rules so the set an environment variable to enable or disable the
touchpad as appropriate.
{{< /alert >}}

## Conclusion

KDE needs an option to turn off the trackpad when a mouse is active, like
other operating systems, or `libinput` needs to get a lot better at ignoring
the touchpad when someone isn't actually using it.

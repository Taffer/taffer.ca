---
title: "Wayland Debugging"
date: 2024-03-29T16:03:55-04:00
draft: false
showHero: true
tags: ["Linux", "OpenSUSE"]
categories: ["Linux"]
---

I'm still trying to figure out why my *Plasma (Wayland)* session just hangs
when I log in via SDDM.

## Setup

{{< alert "lightbulb" >}}
I've been running KDE Plasma 5 with a Wayland session (in Kubuntu 23.04) for
months, but I've got no clue how to figure out *problems* with it, because I
never ran into any. :shrug:
{{< /alert >}}

Ever since my first `zypper dup` update to OpenSUSE Tumbleweed, my
*Plasma (Wayland)* session has been broken (*Plasma (X11)* works fine). When
I log in via SDDM for a *Plasma (Wayland)* session, the screen eventually
turns black. Something in the hand-off between SDDM login and KDE Plasma
starting up is failing.

{{< alert "lightbulb" >}}
If I skip SDDM and go to a console terminal, I can launch a *Plasma (Wayland)*
session by hand (`dbus-run-session startplasma-wayland`), and it *instantly*
fires up properly.
{{< /alert >}}

Trying to find information about debugging
[SDDM](https://wiki.archlinux.org/title/SDDM) hasn't been fruitful; by default,
it'll write `~/.local/share/sddm/wayland-session.log` when starting a Wayland
session. There aren't any SDDM options for debugging, or increasing verbosity.

I haven't been able to find anything useful for
[Wayland](https://wiki.archlinux.org/title/Wayland) or
[KDE](https://wiki.archlinux.org/title/KDE) either.

## Poking around

I managed to get a console terminal after trying to launch *Plasma (Wayland)*,
and stashed the journal:

```sh
journalctl -ab > journal.log
```

I don't see anything obvious around SDDM, Wayland, or Plasma. :shrug:

## From scratch

Created a new user, they can log into the *Plasma (Wayland)* session no
problem. So now I can see if there's some difference in the configs.

I used `kdiff3` to compare `.config` and `.local` between the two accounts, and
didn't see anything suspicious. Then again, I don't really know what I'm
looking for.

Checking SDDM's session logs, and mine doesn't look good.

```sh
~/.local/share/sddm
chrish@gridania$ cat wayland-session.log
/tmp/xsess-env-lbNorC: line 41: PROFILEREAD: readonly variable
```

And the fresh new user account:

```sh
/home/test1/.local/share/sddm
chrish@gridania$ cat wayland-session.log
QIODevice::readLine (QFile, "/home/test1/.config/kdedefaults/package"): device not open
/usr/bin/xrdb: Can't open display ''
/usr/bin/xsetroot:  unable to open display ''
QPixmap: QGuiApplication must be created before calling defaultDepth().
QPixmap: QGuiApplication must be created before calling defaultDepth().
Error: could not determine $DISPLAY.
Error: Can not contact kdeinit5!
org.kde.startup: "kdeinit5_shutdown" QList() exited with code 255
startplasma-wayland: Shutting down...
startplasmacompositor: Shutting down...
startplasmacompositor: Done.
```

I don't have a `/tmp/xsess-env-*` file in my *Plasma (X11)* session, so I'll
have to log out and see if SDDM makes this, or if it's only present during the
SDDM to Wayland handoff.

Here's a
[forum post](https://forums.opensuse.org/t/display-manager-starts-xorg-des-just-fine-but-crashes-when-using-wayland-logs-provided/165298/4)
from someone encountering a similar problem, with the same
`PROFILEREAD: readonly variable` error in the logs. Reading the thread… this
might be caused by using `fish` as my login shell?!

This [KWin Debugging](https://community.kde.org/KWin/Debugging) article might
be helpful, assuming it's up-to-date for the Plasma 6 in Tumbleweed.

## To be continued…

More investigation is required…

## Later that day…

I did a `chsh --shell /usr/bin/bash`, logged out, and logged in with a
*Plasma (Wayland)* session. Started right up in Wayland.

![Captain Picard's iconic face-palm](picard-facepalm.gif)

{{< alert icon="fire" >}}
How the *hell* could my choice of *login shell* break Wayland?! Or is it
KDE Plasma 6 that's broken?!
{{< /alert >}}

## Related Posts

- [OpenSUSE Tumbleweed]({{< ref "/posts/2024/opensuse" >}})
- [OpenSUSE Tumbleweed, Part Two]({{< ref "/posts/2024/opensuse2" >}})

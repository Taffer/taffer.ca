---
title: "Linux Roundup"
date: 2024-05-19T08:25:46-04:00
draft: false
showHero: true
tags: ["Howto", "Kubuntu", "Linux", "OpenSUSE", "Tools"]
categories: ["Linux"]
---

A description of various Linux distros that I've used, which might help someone
else pick one.

## Background

I've used *a lot* of operating systems:

* 8-bits: Vic-20, C=64; brief exposure to Atari 8-bits, Tandy, etc.
* 16-bits: Atari ST for *ten years*; missed out on Amiga somehow, and Acorn
* Mainframes: VMS, RSTS-E, SysV Unix, BSD Unix
* macOS: 6.x, 7.x, OS X across PowerPC, x86, and ARM platforms
* Windows: from Windows 286 to Windows 11 (but not CE or ME)
* OS/2: 2.x (it looked good at the time!)
* BeOS: BeBox 66, uMax s900, Pentium II, Pentium III

I've dabbled with Linux since million-floppy Slackware, and used it (mostly on
servers) at work for ages. A while back, I switched to using Linux on my
personal laptop. IMHO one of the barriers to entry for Linux is the crazy
amount of distros… choosing the "right" one for you is *hard*.

## Why Linux?

Basically, I find current desktop OSes irritating in a death-by-1000-cuts
way; there are too many frustrations in daily use:

* macOS: the Apple Silicon chips are really impressive, but it's not (IMHO) a
  good platform for gaming (or game development), the hardware is insanely
  expensive, and I hate the way things like Xcode work; this is actually my
  second choice platform, I use it for work
* Windows 11: everything MS has done since Windows 8 seems to have been to
  make their OS worse, development is super awkward and painful here, and
  they're jamming "AI" and ads everywhere

What I like about Linux:

* Runs on hardware that I'd actually buy; price out a MacBook Pro with 64GB of
  RAM and 4TB of storage, for example. I got 64GB of RAM and 4TB of m.2 NVME
  for something like $800 CAD *and* I wasn't forced to buy the top-end CPU.
* If something breaks, or needs tweaking, I can figure out how to fix it.
  Usually. Eventually.
* I can choose a non-awful UI that doesn't tie me to the cloud, or jam LLMs and
  ads in my face everywhere. None of them helpfully included an icon for Candy
  Crush (which I played on a mobile device, why would I want it on my laptop).
* So very comfy for software development, and my preferred game dev platform
  can export to macOS and Windows anyway.
* Works great for gaming, at least for the games I play. The Steam Deck has
  really helped out here!
* I do miss the excellent [Affinity](https://affinity.serif.com/en-us/) Photo
  and Designer, which do work on macOS or Windows. I didn't use either one
  enough for them to tie me to Windows. Pity they don't work in Wine though.

## What I Look For

The features I consider important include:

* Up-to-date software and kernel; the "stable" distros that lag behind by
  years *really* annoy me for some reason.
* I want to use BTRFS as my root filesystem. I know it's not (quote) the same
  old, same old, but ext4 feels like a primitive filesystem to me.
* Needs to run all the software I depend on daily, all the software I want to
  use for development, *and* the games I want to play.
* I probably want to use KDE as my desktop environment. It's super-customizable,
  but also the defaults are pretty decent.
* I want to use Wayland instead of X11. Wayland is *good enough* now, and I
  don't need any of the missing features that X11 supported (except proper
  screen savers; I miss [XScreenSaver](https://www.jwz.org/xscreensaver/)).

## Find Your Own

Don't like my choices, or want to explore your options based on *your* needs?
That's a great idea! There are some distribution choosers that lead you through
a short questionnaire and then try to divine which distros will work well for
you:

* [Distrochooser](https://distrochooser.de/) - The original (?). Tells me to
  use Linux Mint.
* [DistroChooser but Better!](https://distrochooser.snehit.dev/) - A better
  (?) Distrochooser. Suggests Endeavour OS (an Arch flavour).
* [LibreHunt](https://librehunt.org/) - Some intro info, then a chooser.
  Gave me Solus, which I don't think I've heard of before.
* [Fushra distro picker](https://distros.fushra.com/) - Suggests Manjaro's KDE
  flavour.

All of them will give you a bunch of results, so you can choose something that
appeals to you.

## Flavours of Linux

There are more Linux distros than grains of sand in the Sahara. Switching
between them pretty much requires starting over with a freshly formatted
system. How are you supposed to pick the "right" one?

I'm going to write down some things about the flavours I've tried in the past
five-ish years, and hopefully it'll let you pick one you like. They're in order
of when I used them, which is why the first four aren't sorted properly.

Things I've been using as "daily drivers" on my laptop:

* [Ubuntu](https://ubuntu.com/)
* [Linux Mint](https://linuxmint.com/)
* [Kubuntu](https://kubuntu.org/)
* [OpenSUSE Tumbleweed](https://get.opensuse.org/tumbleweed/)

Things I've also looked at:

* [Arch]((https://archlinux.org/))
* [Manjaro](https://manjaro.org/)
* [Fedora](https://fedoraproject.org/)

### Ubuntu

{{< figure
  src="Canonical_Ubuntu.svg"
  alt="Ubuntu's logo"
  default=true
  width="25%"
  >}}

[Ubuntu](https://ubuntu.com/)'s a popular desktop Linux, and one I've used a
bunch for servers. It's solid, and it's usually targeted for binary software
releases.

Pros:

* ✅ Easy to install, easy to get help with.
* ✅ Lots of software available.
* ✅ Regular release schedule between long-term (LTS) and normal? unstable?
  releases.

Cons:

* ❌ Over-reliance on "Snaps", a kind of containerized distribution system
  for applications. I don't like how Snaps interact with the rest of the
  system, at least on a desktop.
* ❌ The Gnome desktop caught the "Hey, we should make a mobile UI for
  *computers*!" bug that made Windows 8 so awful. For me, this is awkward
  to use on a laptop. Of course, you might prefer Gnome, I won't judge.
* ❌ Still uses ext4 as the base filesystem.

### Linux Mint

{{< figure
  src="Linux_Mint.svg"
  alt="Mint's logo"
  default=true
  width="40%"
  >}}

[Linux Mint](https://linuxmint.com/) is based on Ubuntu, but with a custom
desktop environment that's more like "classic" Windows. This distro is
considered *really* easy for Windows users to pick up.

Pros:

* ✅ See Ubuntu.
* ✅ Easy to install, easy to get help with (because it's mostly Ubuntu
  under the hood).
* ✅ Better desktop environment than Gnome.
* ✅ Regular release schedule that mirrors Ubuntu's.
* ✅ Familiar to Windows users.
* ✅ Less reliance on Snaps.

Cons:

* ❌ See Ubuntu.
* ❌ Releases lag behind Ubuntu (of course).
* ❌ Release upgrades are fraught with peril; AFAIK they still suggest doing
  a fresh install rather than upgrading your existing system.

### Kubuntu

{{< figure
  src="Kubuntu.svg"
  alt="Kubuntu's logo"
  default=true
  width="40%"
  >}}

[Kubuntu](https://kubuntu.org/) is Ubuntu with the KDE desktop environment.

Currently, this is the Linux distro I've used the longest on my personal
laptops. I did go with OpenSUSE for my new laptop though…

Pros:

* ✅ See Ubuntu.
* ✅ KDE is a much nicer (IMHO) desktop environment that's easier to set up
  and tweak the way I want.

Cons:

* ❌ See Ubuntu.

### OpenSUSE Tumbleweed

{{< figure
  src="OpenSUSE_Tumbleweed.svg"
  alt="Tumbleweed's logo"
  default=true
  width="33%"
  >}}

OpenSUSE is a distro that's popular in Europe, and
[Tumbleweed](https://get.opensuse.org/tumbleweed/) is their rolling release;
a rolling release ships with all of the current, up-to-date, and hopefully
stable versions of everything, and push out updates whenever new versions are
available.

Pros:

* ✅ Rolling release! Current kernel, current software.
* ✅ Uses BTRFS as the root filesystem.
* ✅ Lots of documentation, forums, etc. for finding information or getting
  help.
* ✅ KDE desktop environment.
* ✅ Large amount of software available, and you can install packages from
  `.rpm` files (the RedHat Package Manager that Fedora and other RedHat
  derivatives use). Usually.

Cons:

* ❌ Rolling release! You might get unstable versions of software. This hasn't
  affect me (yet!), but it's always a risk with rolling releases.
* ❌ Cutting edge everything means a lot of the existing documentation or
  forum advice will be out of date compared to what you're actually using.
* ❌ Sometimes, installing from foreign `.rpm` files runs into dependency
  problems. I've run into this trying to get Docker Desktop installed; luckily
  the command-line Docker installs and runs fine, but I use it infrequently
  and would prefer the GUI to hold my hand.

### Arch

{{< figure
  src="Arch_Linux.svg"
  alt="Arch's logo"
  default=true
  width="50%"
  >}}

> I use Arch, by the way. - Any random Arch user, any time.

Pros:

* ✅ Rolling release! Arch is the iconic rolling release distro.
* ✅ Set it up any way you want.
* ✅ *Outstanding* documentation in the Arch wiki, they've done a great job
  here. The information isn't generally Arch-specific, so it's a valuable
  reference for any Linux user.

Cons:

* ❌ No real installer, you set everything up the way you want. So you have to
  know what you're doing already, and you might have to plan it out before you
  start. (There's a text-based installer thing, but it leaves the `root`
  password in a plaintext log file on the resulting system. D'oh!)
* ❌ Rolling release! You might get unstable software, and you might get
  stable software.

### Manjaro

{{< figure
  src="Manjaro.svg"
  alt="Manjaro's logo"
  default=true
  width="50%"
  >}}

A friendly OS for desktop users, based on Arch.

Pros:

* ✅ Rolling release!
* ✅ Nice graphical installer.
* ✅ Arch's foundation and documentation.

Cons:

* ❌ Only *based on* Arch, not Arch with a nice installer. It's unclear how
  and where this diverges from Arch, at least to me.
* ❌ Rolling release!

### Fedora

{{< figure
  src="Fedora.svg"
  alt="Fedora's logo"
  default=true
  width="37%"
  >}}

An community version of RedHat's Enterprise Linux. A *lot* of distros have
Fedora as their ancestor.

Pros:

* ✅ Stable releases.
* ✅ Graphical installer.
* ✅ Lots of existing documentation, community, software, etc.

Cons:

* ❌ IBM owns RedHat, which makes Fedora's future uncertain; IBM is clearly
  focused on squeezing money out of customers, and Fedora *costs* money, it
  doesn't *bring in* money.
* ❌ Slow release schedule, so you're always stuck with out-of-date (IMHO)
  software. Other people would put this in the Pros section because it gives
  you a *very* stable environment.

## Image Credits

None of these images are mine; [let me know](mailto:chrish@pobox.com) if I've
got the attributions wrong and I'll be happy to change them!

* [Arch Linux](https://archlinux.org/) logo:
  [Arch art page](https://archlinux.org/art/)
* [Fedora Linux](https://fedoraproject.org/) logo: Wondigoma on
  [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Fedora_logo.svg)
* [Kubuntu](https://kubuntu.org/) logo: the
  [Kubuntu website](https://kubuntu.org/wp-content/uploads/2024/04/3099/Horiz-Full-Text.svg)
* [Linux Mint](https://linuxmint.com/) logo: the
  [Linux Mint brand-logo repo](https://github.com/linuxmint/brand-logo)
* [Manjaro Linux](https://manjaro.org/) logo: the Manjaro developers on
  [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Manjaro-logo.svg)
* [OpenSUSE Tumbleweed](https://get.opensuse.org/tumbleweed/) logo: Richard
  Brown and Zvezdana Marjanovic,
  [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:OpenSUSE_Tumbleweed_green_logo.svg)
* [Tux](https://commons.wikimedia.org/wiki/File:Tux.svg) the penguin: CC0 made
  by Larry Ewing.
* [Ubuntu Linux](https://ubuntu.com/) logo: Ubuntu's
  [brand page](https://design.ubuntu.com/brand)

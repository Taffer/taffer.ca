---
title: "Obsidian vs Notesnook"
date: 2024-02-23T12:12:28-05:00
draft: false
tags: ["notes"]
categories: ["Work", "Gamedev"]
---

Comparing Obsidian and Notesnook, since my Notesnook sync trial is probably
almost up.

# Background

I'm an ADHD person and one of my coping mechanisms is to take notes. If I
write it down, it's out of my brain *and* I won't forget it.

I used to carry around a physical notebook and pen, which worked OK but was
annoying in the hot season when I didn't have spare jacket pockets handy.
Accessing and searching these notes was also less than awesome.

## EverNote

I used [EverNote](https://evernote.com/) for a while before it started getting
terrible. Being able to access my notes "anywhere" was great, and organizing
them was better than paper notes. It fell down pretty hard while I was trying
to take notes for cryptography classes, but was fine for simple things.

EverNote has since gotten worse and worse as the company's owners try to
squeeze money out of the user base (I'm happy to pay for things that are of
value to me). Also, no Linux version.

## OneNote

Around the time they released a Mac version, I switched to
[OneNote](https://www.onenote.com/) which IIRC didn't sync to OneDrive at the
time (or it wasn't mandatory), and which didn't have some sort of mobile device
access… the editing experience was so much better! Then they added OneDrive
support (nice) and rewrote the whole thing as a UWP app, discarding about half
of the original features and completely ruining the UI from an organizational
perspective. The "modern" UI used about twice the amount of screen space to
display maybe half the amount of information.

If there was a Linux version of OneNote (especially with the UI/features from
around 2010) I'd consider switching back to this even though it only stores
your data in OneDrive these days. There's a thing that gives  you a dedicated
window to the online version of OneNote from Linux, but it's not great.

## Joplin

Eventually, I switched to [Joplin](https://joplinapp.org/), which has some nice
features/abilities:

* sync to whatever cloud I felt like using
* encrypted data, where I control the key
* write in Markdown
* open source

Sync was a bit iffy at times, and getting it to download/decrypt my entire
note database was surprisingly difficult on mobile devices.

## Obsidian

Next up was [Obsidian](https://obsidian.md/), which is a closed-source program,
but pretty great:

* sync to their cloud (somewhat expensive)
* no encryption (the "end to end encryption" in their sync features just
  means TLS)
* write in Markdown
* you can store the note "vault" wherever you want

Since the vault is just a directory hierarchy of Markdown files (*awesome!*),
and you can store it wherever you want, I've been keeping mine in a
[pCloud](https://www.pcloud.com/) directory that syncs automatically between
the (desktop) platforms that I use. There are merge conflicts sometimes, and I
have to use some hackery to sync it with a mobile device, so not ideal.

## Notesnook

I stared mucking around with [Notesnook](https://notesnook.com/), which seems
pretty good:

* open source clients and server
* sync to their cloud (more reasonably priced than Obsidian's)
* open-ish development process
* notes are encrypted before being sync'd
* write in Markdown, sort of; the editor supports Markdown "shortcuts", but
  the notes are stored in some other format

# So, which should I use?

Since my free sync trial with Notesnook is coming to an end, I should figure
out a few things:

* What are the actual "must have" features for me in a note system?
* How does Obsidian + pCloud compare to Notesnook?
* I should probably include Joplin in this, too?

## Feature Comparison

Clearly I need to figure out what features are important to me, and how well
each provides them. Note that these apps all support a *lot* of additional
features that might be more important to *you* than they are to *me*, so take
a look at their feature lists and adjust opinions for your own needs.

* Platform support: I currently need macOS, Linux, and iOS access to my notes.
    * Obsidian: macOS ✅, Linux ✅, iOS ✅; since I'm using pCloud to sync,
      I've got limited access to things on iOS. There's an add-on that tries
      to sync with other clouds (I think I was using OneDrive), but it doesn't
      seem to be reliable, at least the way I've been using Obsidian.
    * Notesnook: macOS ✅, Linux ✅, iOS ✅; sync appears to work great
    * Joplin: macOS ✅, Linux ✅, iOS ✅; getting sync to actually complete
      (I have around 500 notes, plus attached images) on mobile devices seems
      very iffy

* Writing: I really like writing in Markdown. It's usually enough for what I
  do, and there are usually plugins for the rare times I need more (usually
  diagrams or LaTeX-style symbols and whatnot).
    * Obsidian: Notes are just a hierarchy of folders and Markdown files
      wherever you've chosen to store them in your filesystem.
    * Notesnook: Notes are something, you can use Markdown style shortcuts
      while writing in the editor. It doesn't currently support organizing
      your notes in a hierarchy… all the notes go in a big pile, and you can
      "link" them to a notebook, or tag them.
    * Joplin: Markdown is the native note format, organized in a hierarchy
      of notebooks/folders.

Hmm, I sort of stalled out here. The feature sets of all three are pretty
similar… the real questions might just be:

* How well does this support mobile access?
  * Obsidian: Expensive-ish cloud, or an add-on that seems to cause a lot of
    merge conflicts.
  * Notesnook: More reasonably-priced cloud, app seems pretty good on mobile.
  * Joplin: Cloud of your choice, unreliable syncing with mobile.
* How often do I actually need mobile access?
  * For taking notes: I can usually just drop the idea or phrases or whatever
    into my todo list app, and then make a proper note later when I'm at my
    laptop.
  * For reading notes: This is hard to predict. A lot of what I need "on the
    run" can be in my todo app, but sometimes I do need (or like to have)
    access to longer notes about things, like travel information for example.
    Definitely not a constant or frequent need, but useful when I do need it.

# Conclusion

I still have no idea. I'm happy/comfortable writing/reading in all three, and
it looks like Notesnook's weak organizational powers are being addressed in
future releases (I just don't know when those will be happening).

Inertia will keep me using Obsidian and storing my notes in pCloud, but I
should take another look at Joplin's cloud support and also see if I can
figure out more about Notesnook's release schedule (their
[roadmap](https://notesnook.com/roadmap/) is public, and lists "Nested
notebooks/topics" as "Pending release", whatever that means).

Hopefully this was mildly interesting, but I feel like I've just wasted some
of your time, and mine. At least I got this out of my brain?

---
title: "Trying to WordPress"
date: 2024-01-06
draft: false
tags: ["WordPress"]
categories: ["Website"]
aliases: ["/trying-to-wordpress/"]
---

I'm going to try doing my blog in WordPress now that I've got a "real" website.
I might just nuke everything and go back to hand-crafted HTML, we'll see.

If I can figure out how to change the posting date on things, I'll copy my
existing blog entries (there are only a few) here, if not, they're still on
CodeBerg:

[https://codeberg.org/Taffer/Blog](https://codeberg.org/Taffer/Blog)

## Updates

### 2024-01-06 @ 10AM

I don't particularly like the styling yet, it's way too huge with too much
scrolling. More mucking around is required.

I also need to fix up a bunch of links and formatting in the other posts I've
pulled in, the Markdown plugin I'm using honours line breaks for some
reason…

### 2024-01-18

Moving this into a [Hugo](https://gethugo.io)-based static website, since
WordPress is way too heavy for what I need. And I an keep writing my stuff
in Markdown…

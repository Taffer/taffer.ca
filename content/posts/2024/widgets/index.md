---
title: "Widgets"
date: 2024-08-26T16:04:08-04:00
draft: false
showHero: true
tags: ["Widgets", "Linux"]
categories: ["Linux"]
---

A short story of frustration and spite, because this shouldn't be so difficult.

## What's a Widget?

Ever since [Konfabulator](https://en.wikipedia.org/wiki/Yahoo!_Widgets) and
[Google Desktop](https://en.wikipedia.org/wiki/Google_Desktop#Desktop_Gadgets)
became available for the desktops I was using, I've loved having them; I really
like having an analog clock I can glance at, info about system resource usage,
and a weather forecast handy. These little bits of UI sitting on your desktop
are [*widgets*](https://en.wikipedia.org/wiki/Software_widget).

Mobile platforms support them too, on your lock screen or the home screen. I've
got ones showing the weather and an analog clock on my phone.

Current systems that support widgets include:

- Apple platforms (I'm not sure about Watch or TV)
- Android (at least on phones/tablets, I'm not sure about other devices)
- Windows (various versions have various different widget systems)
- KDE Plasma on Linux
- Cinnamon on Linux

The idea behind widgets was to be able to easily create some sort of
display/tool/game/etc. that wasn't necessarily available as part of the OS, in
a handy, accessible (and even attractive) way. They're a way of customizing
your desktop the way *you* want it.

## Making Widgets

When I used Windows, I *really* liked [Rainmeter](https://www.rainmeter.net/),
and especially the great
[Gadgets by SilverAzide](https://github.com/SilverAzide/Gadgets) set
of widgets (aka "skins" in Rainmeter-ese). This is actually one of the few
things I miss from the platform.

![Screen shot of Rainmeter Gadgets, shamelessly stolen from SilverAzide's GitHub repo](GadgetsMain.png)

I'm currently using [OpenSUSE](https://www.opensuse.org/) linux with the
[KDE Plasma 6](https://kde.org/) desktop. KDE's got a selection of widgets,
including the kind I normally like to have on my desktop: analogue clock,
system monitors for RAM/CPU/disk usage, and a weather forecast. Unfortunately,
I find these *huge* and unattractive.

![Screen shot of my KDE desktop, with the right edge of the screen full of widgets](featured.png)

I know! I'll just recreate those Rainmeter Gadgets for Plasma, how hard could
it be?

### A Survey of Modern Widget Platforms

As it turns out, it's apparently extremely hard, for several reasons:

- KDE's [Plasmoids](https://develop.kde.org/docs/plasma/widget/): All of the
  documentation and tutorials are for creating Plasma 5 widgets; Plasma 6
  widgets have changed, and the changes aren't well-documented, because they
  extend into the QML used to implement them. Oh, and you need to know
  [Qt](https://contribute.qt-project.org/) already before you stand a chance of
  effectively using QML.
- Cinnamon's [Desklets](https://cinnamon-spices.linuxmint.com/desklets): There
  is no documentation, there are no tutorials. There's a repo containing the
  source for all of the available Desklets, but again, good luck unless you
  already know what you're doing.
- [macOS/iOS/etc.](https://developer.apple.com/documentation/widgetkit/): These
  widgets are an exposed bit of an application, which is probably fine if
  you've already got an application written.
- [Windows](https://learn.microsoft.com/en-us/samples/microsoft/windowsappsdk-samples/widgets/):
  Again, exposed bits of an application.

I've found a few abandoned-looking projects for displaying widgets, but none of
them seem to have had any traction, so there are a small handful of widgets and
no on-going development.

### A Bad Idea

It'd sure be nice if there was a portable widget engine, where you could use
the same widgets on whatever platform you wanted. And those widgets could just
be made using standard, common technologies like HTML, CSS, and JavaScript.
Everyone knows those, they're relatively easy to use… we'd finally get back to
anyone being able to create whatever widgets they wanted!

That seems like a great idea, *and* seems like less work than
reverse-engineering Plasmoids or Desklets. Surely I would never regret starting
another project because I'm annoyed by the currently existing projects and/or
the state of their documentation!

Before I started down this road, I actually looked around for similar projects.
I doubt I found everything (for example, I totally missed
[hudkit](https://github.com/anko/hudkit)
until [@unspeaker@mastodon.social](https://mastodon.social/@unspeaker) pointed
it out). Then I spent some time looking for reasonable platforms to build this
in.

### Thoth

Because I didn't find anything I liked, I started the
[Thoth widget engine](https://widgets.taffer.ca/).
It's currently at the design-and-mucking-around stage, but it's already got
[one widget](https://codeberg.org/Taffer/thoth-hello-world)
(just a Hello World I made).

![Hello World screen shot](ThothHelloWorld.png)

Thoth will:

- be portable to "any" desktop platform (Linux of course, but macOS and Windows
  will also be there, barring build/code signing problems)
- use standard web technologies for developing widgets: the HTML, CSS, and
  JavaScript you know and love/hate
- a minimal set of APIs specific to Thoth, so your widgets can access system
  bits (settings, system data in a platform-neutral way, probably a bit of
  platform-specific information as well)
- be extensively and exhaustively documented, with working tutorials

After looking at things like [GTK's WebKit integration](https://webkitgtk.org/),
[Qt's web stuff](https://doc.qt.io/qt-6/qtwebengine-overview.html),
[Servo](https://servo.org/), and a few other things, I gave up and started work
with [Electron](https://www.electronjs.org/). Sure it's huge, but it sort of
makes sense… Thoth is effectively a limited web browser.

I'm not a JavaScript developer, so this route has exposed me to a world of pain
and frustration that I'm really not interested in learning. All of the tooling
and frameworks and whatnot seem to assume you're already deep in the ecosystem
and you know what you're doing. I sure don't know what I'm doing here. Why do
all the Node docs for anything talk about `npm` *and* `yarn`? I have no idea,
but there seems to be forty different ways to do everything, and no way to pick
the "right" one. 🤷

Now I'm looking into [Tauri](https://tauri.app/), which is sort of like
Electron, but you're implementing the back-end in
[Rust](https://www.rust-lang.org/) (which I've wanted to learn anyway), and it
uses the platform's web implementation (GTK's WebKit on Linux, native WebKit on
macOS, and Edge on Windows) instead of bundling a full web browser.

## Watch It Happen

If you're at all interested in following the saga of a portable widget engine
using standard technologies for the widgets, you can see what's going on with
Thoth in a few places:

- [Thoth's website](https://widgets.taffer.ca/)
- [Thoth's code repo](https://codeberg.org/Taffer/thoth)
- [Thoth's Mastodon account](https://fosstodon.org/@thothwidgets)

Note that you don't need an account to view any of those (although I do
recommend [joining Mastodon](https://joinmastodon.org/) on a server associated
with your interests, so you can escape *The Algorithm* and endless
advertising).

---
title: "Wayland Debugging 2 - The Debuggening"
date: 2024-06-26T07:03:31-04:00
draft: false
showHero: true
tags: ["Linux", "OpenSUSE"]
categories: ["Linux"]
---

Part of the "fun" (?) of running a rolling release is that you might sometimes
be left with a partially broken system; today it's Plasma 6 in Wayland (again).

**Update:** The next big Tumbleweed update included new versions of the Mesa
packages. Wayland is happy again on my laptop.

## Previously

In March I switched my default shell from `bash` to
[`fish`](https://fishshell.com/), which prevents KDE Plasma 6 from launching.
[Of course it does.](/posts/2024/wayland/). That might only affect Wayland
sessions, I don't remember, and I don't care, because that's a
[stupid bug](https://forums.opensuse.org/t/display-manager-starts-xorg-des-just-fine-but-crashes-when-using-wayland-logs-provided/165298/4).

## Today

Big OS update (via `sudo zypper dup`), so I reboot to grab the new kernel and
whatnot. Attempting to log in via SDDM to my usual Plasma (Wayland) session
makes the screen flicker (looks like an uninitialized frame buffer), then
kicks me back to SDDM.

Switch to Plasma (X11), and it works.

My `~/.local/share/sddm/wayland-session.log` has nothing useful:

```log
Error: could not determine $DISPLAY.
Error: Can not contact kdeinit5!
org.kde.startup: "kdeinit5_shutdown" QList() exited with code 255
startplasma-wayland: Shutting down...
startplasmacompositor: Shutting down...
startplasmacompositor: Done.
```

### Weird Things

A couple of weird things with the X11 session:

* scrolling is *painfully* slow
* I can see VSCode drawing sometimes
* fans run a *lot* more, even without doing any 3D

I haven't tired running any games (yet) because the critically-acclaimed
MMORPG is down for a two-day maintenance ahead of releasing its latest
expansion.

Maybe the APU isn't being used? `neofetch` says it's still there, so hopefully
the AMD drives aren't too wonky.

## Debugging

[This page](https://community.kde.org/KWin/Debugging) has a section on debugging
crashes on login, and suggests running these commands from a text console:

```sh
export QT_LOGGING_RULES="kwin_*.debug=true"
startplasma-wayland > ~/startplasma-wayland.log 2>&1
```

Sounds good, and `startplasma-wayland` dies a lot faster this way than it does
from SDDM. Except `startplasma-wayland.log` has the same useless messages we
saw before:

```log
Error: could not determine $DISPLAY.
Error: Can not contact kdeinit5!
org.kde.startup: "kdeinit5_shutdown" QList() exited with code 255
startplasma-wayland: Shutting down...
startplasmacompositor: Shutting down...
startplasmacompositor: Done.
```

This is basically the same amount of information I got
[earlier this year.](/posts/2024/wayland/)

`journalctl -b | grep -i wayland` gives me a pile of output, starting with
this tidbit, which seems to suggest I've been bitten by a rolling release
problem… looks like `kwin_wayland_wrapper` is compiled against a different
Mesa build than what I've got installed. Awesome.

```log
Jun 26 12:25:01 gridania ksmserver-logout-greeter[29696]: qt.qpa.plugin: Could not load the Qt platform plugin "wayland" in "" even though it was found.
Jun 26 12:25:52 gridania dbus-daemon[2602]: [session uid=1000 pid=2602] Activating service name='org.kde.KSplash' requested by ':1.523' (uid=1000 pid=30129 comm="startplasma-wayland")
Jun 26 12:25:53 gridania kwin_wayland[30153]: kwin_watchdog: Watchdog: disabled, not running on a systemd environment or watchdog is not set up. No WATCHDOG_USEC.
Jun 26 12:25:53 gridania kwin_wayland[30153]: No backend specified, automatically choosing drm
Jun 26 12:25:53 gridania kwin_wayland_wrapper[30153]: DRI driver not from this Mesa build ('24.1.1' vs '24.1.2')
Jun 26 12:25:53 gridania kwin_wayland_wrapper[30153]: failed to bind extensions
Jun 26 12:25:53 gridania kwin_wayland_wrapper[30153]: DRI driver not from this Mesa build ('24.1.1' vs '24.1.2')
Jun 26 12:25:53 gridania kwin_wayland_wrapper[30153]: failed to bind extensions
Jun 26 12:25:53 gridania kwin_wayland_wrapper[30153]: DRI driver not from this Mesa build ('24.1.1' vs '24.1.2')
Jun 26 12:25:53 gridania kwin_wayland_wrapper[30153]: failed to bind extensions
Jun 26 12:25:55 gridania kwin_wayland_wrapper[30153]: DRI driver not from this Mesa build ('24.1.1' vs '24.1.2')
Jun 26 12:25:55 gridania kwin_wayland_wrapper[30153]: failed to bind extensions
Jun 26 12:25:55 gridania kwin_wayland_wrapper[30153]: DRI driver not from this Mesa build ('24.1.1' vs '24.1.2')
Jun 26 12:25:55 gridania kwin_wayland_wrapper[30153]: failed to bind extensions
Jun 26 12:25:55 gridania kwin_wayland_wrapper[30153]: DRI driver not from this Mesa build ('24.1.1' vs '24.1.2')
Jun 26 12:25:55 gridania kwin_wayland_wrapper[30153]: failed to bind extensions
```

I think I need to wait for Tumbleweed to update Mesa? Surely there's some
better way to handle this than just crashing back to SDDM when everything
fails?

Starting to think that going with a rolling release was a bad choice, but I was
really hoping to have up-to-date software that doesn't lag reality by
weeks/months/years.

## Related Posts

* [Wayland Debugging](/posts/2024/wayland/)

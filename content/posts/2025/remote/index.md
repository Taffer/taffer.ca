---
title: "Remote Work, 2025 Edition"
date: 2025-01-18T15:41:27-05:00
draft: false
showHero: false
tags: ["Remote", "WFH"]
categories: ["Work"]
aliases: ["/remote-work/"]
---

A collection of job search sites and specific companies that are remote-first
or remote-friendly, to help everyone be happier with their work.

In 2023, I [made a post](/posts/2023/remote/) to help people find jobs that
don't contribute to the climate crisis by forcing them to commute to an
office and expose themselves to COVID-19 and other airborne diseases. I felt
a need to re-organize and clean up, so here's a fresh new edition for people
looking for work in 2024, and I'm doing it again this year to keep the
information fresh.

**Last updates:** 2025-03-12

Let me know if I'm missing any:

- Job sites or search engines for remote work.
- Companies who are remote-first, or remote-friendly.
- Companies who *used to* do remote work, but who have forced everyone into
  offices; I'll remove them ASAP.
- Correct Glassdoor links (a *lot* of companies have similar names).

You can contact me through an
[Issue](https://codeberg.org/Taffer/taffer.ca/issues) on CodeBerg, via
[email](mailto:chrish@pobox.com), or on
[Mastodon](https://mastodon.gamedev.place/@Taffer). I'm `the_real_taffer`
🙄 on Discord if that's your thing, just remember to give me some context
before dumping data.

**NOTE:**  I'll keep updating this as long as I keep getting new information,
or until I make a new edition!

{{< alert >}}
**Warning:** The companies listed in
[this Google doc](https://docs.google.com/spreadsheets/d/1nW7kbqVz8XUCRFEgH5Y60YzmoOfl7IM9w4DAhb2kcX4/edit?gid=0#gid=0)
have apparently been posting "ghost jobs". These are job postings where there's
no intent to hire. There are various sketchy reasons for doing this, and none
of those reasons help *you* in any way.
{{< /alert >}}

## List Sites

There are some other remote company lists that probably include places I
haven't got here (my list leans towards tech companies):

- [remoteintech.company](https://remoteintech.company/)'s *really* long list;
  not sure how up-to-date this is, as it lists Amazon (definitely *not*
  remote-friendly).
- [Remotive](https://remotive.com/remote-companies) (they've also got a
  [Google Doc](https://docs.google.com/spreadsheets/d/18bljq3y5YTxPLmA4xj10EaKxs1KWhIdLTr8bF1K5XKI/edit?pli=1&gid=0#gid=0)
  with about 170 companies listed, but it's mostly just an ad for this job site;
  they've got a membership with a one-time fee)
- [Yanir Seroussi's great list](https://github.com/yanirs/established-remote);
  this has a ton of useful information (business/product, tech stack, etc.)
  that I'll never have time to add to my list.

## Search Sites

Job-finding sites specializing in remote work:

- [Arc.dev](https://arc.dev/) - in 2024-11 they had a
  [blog post](https://arc.dev/talent-blog/remote-first-companies/) about
  remote-first companies
- [AwwCor.Inc](https://awwcor.com/jobboard)
- [Builtin](https://builtin.com/) lets you filter by Remote only
- [Communitech](https://www1.communitech.ca/jobs) (their "Remote" filter may
  include "Hybrid")
- [crossover.com](https://www.crossover.com/)
- [findasync.com](https://findasync.com/)
- [Flexjobs](https://www.flexjobs.com/remote-jobs) (not useful without a paid
  account, with a monthly fee) - Be sure to check anything you find here on
  Glassdoor; they listed at least one
  ([Clio](https://www.glassdoor.ca/Reviews/Clio-Reviews-E713360.htm)) who've
  done a hard RTO.
- [Funded.club](https://funded.club/startupjobs)
- [HiringCafe](https://hiring.cafe/)
- [Jobgether](https://jobgether.com/search-offers)
- [Jobspresso](https://jobspresso.co/)
- [nodesk.co](https://nodesk.co/)
- [OpenSource Job Hub](https://opensourcejobhub.com/) is specifically for jobs
  focusing on open source work, many of which are remote.
- [Remote.co](https://remote.co/) (not useful without a paid account)
- [remote.io](https://remote.io/)
- [Remote|OK](https://remoteok.com/)
- [Remote Rocketship](https://www.remoterocketship.com/)
- [Remotive](https://remotive.com/) (requires subscription)
- [VirtualVocations](https://www.virtualvocations.com/)
- [Virtustant](https://jobs.virtustant.com/)
- [weworkremotely.com](https://weworkremotely.com/)
- [workingnomads.com](https://www.workingnomads.com/jobs)

### B Corp Jobs

The B Corp certification is for "beneficial" (thus, the *B*) companies:

> Certified B Corporations are businesses that meet the highest standards of
> verified social and environmental performance, public transparency, and legal
> accountability to balance profit and purpose.

A list of Canadian certified B Corp companies:
[https://bcorpdirectory.ca/explore/](https://bcorpdirectory.ca/explore/)

A list of American certified B Corp companies:
[https://www.bcorporation.net/en-us/find-a-b-corp/](https://www.bcorporation.net/en-us/find-a-b-corp/)

These seem more inclined to support remote work, and to generally not be
awful.

### Green Jobs

These three job search sites specialize in "green" companies; I haven't verified
that they're all remote, but I'd expect that to be a strong possibility:

- [MCJ Collective](https://www.mcjcollective.com/)
- [Terra.do](https://terra.do/)
- [Work On Climate](https://workonclimate.org/)

### Charity Jobs

Here's a job search for charities and other nonprofits; not necessarily
remote, but still a good thing:

- [CharityVillage](https://charityvillage.com/)

## Remote Companies

Previously, I organized the companies into "Remote First" and "Remote
Friendly"; this was getting unwieldy, so I've merged them into one.
"Remote First" are mostly companies that don't have traditional offices, while
"Remote Friendly" are companies that have offices and usually offer in-office
or "hybrid" positions (2-3 days/week in office) as well as fully remote.

Some positions, usually for unicorn applicants (that is, people who have
really specialized skills and experience), can be *made* remote, even at
old fashioned companies. But don't count on it.

|Glyph|Meaning                                       |
|-----|----------------------------------------------|
|🇨🇦   |A Canadian company (very in-progress!).   |
|🐝   |A "B" corporation.                            |
|🥇   |A remote-first company.                       |
|ℹ️   |A link to Glassdoor's reviews for the company.|

**NOTE:** You may have to log in to view the Glassdoor reviews, and they may be
up to some
[shady things](https://arstechnica.com/tech-policy/2024/03/glassdoor-adding-users-real-names-job-info-to-profiles-without-consent/)
with the social network thing they bought. I'm not aware of another way (beyond
insiders and your own networks) to get an idea about what the companies are
actually like to work for.

{{< alert icon="poo" >}}
Looks like Glassdoor has decided to make everything worse and try harder to
become LinkedIn, so I'm considering removing the Glassdoor links (but I
probably won't because I don't know of a useful replacement other than
word of mouth through your own networks):

![A cropped screenshot from Glassdoor in my web browser; they're blocking me from using the site because I haven't "shared anything" in the past 12 months. To "restore access" I need to post a review or some salary information.](Screenshot_20250302_065326.png)
{{< /alert >}}

{{< company-list "all-remote.json" >}}

## Related Posts

- [Remote Jobs (2024 Edition)](../../2024/remote/)
- [Remote Jobs (2023 Edition)](../../2023/remote/)

---
title: "Canadian Hosting"
date: 2025-02-15T06:51:08-05:00
draft: false
showHero: true
tags: ["Upgrades", "Hosting"]
categories: ["Tools", "Website", "Canada"]
---

Some thoughts about Canadian email, web, and cloud hosting.

Wow, I haven't written a blog post since last September's sarcastic
[Internal Tools](/posts/2024/internal-tools/) writeup. Things have been
going fine with my laptop, etc. and I don't generally write about work or
politics or whatever.

For *some reason* I've been looking to move a few things from American
hosting companies to Canadian (or European) companies lately, so I thought
I'd write down a few things in case other people are too. I even activated
my [CIRA](https://www.cira.ca/) membership (free if you own a `.ca` domain).

{{< alert "comment" >}}
**2025-02-21 Update:** Here are links to European alternatives to common
services and a related list of ad-free privacy tools that lists some useful
alternatives:

* [European alternatives](https://european-alternatives.eu/alternatives-to)
* [Ad-free privacy tools](https://www.privacyguides.org/en/tools/)

**2025-02-27 Update:** I couldn't find a collection of Canadian alternatives,
so I started one: <https://codeberg.org/Taffer/canadian-alternatives>

Also, I'd made an incorrect assumption about Fastmail (they're actually
Austrailian!), so I've fixed that.
{{< /alert >}}

## It Begins

I actually started this before the current LLM "AI" bubble's grifters decided
copyright didn't apply to them. I moved my usual cloud storage out of the
US, for example, and I don't use much of anything that's a cloud-only web
application.

So this isn't a new thing for me, but it's become more of a focus in the last
month. It is a mystery!

And it's a shame, because I like the ones I'm still using and have been happy
with their services.

## Website and Email

For ages I've had my website and email hosted on Dreamhost, and things have
been solid. The website is easy as it's totally static and low-traffic, but
we've got a lot of email, going back to freelance work I did in the 90s.
Maybe I should clean that up.

I've also used PObox.com for decades to forward email to my real email address;
this was extremely handy back when switching ISPs required you to update all
your online accounts, etc. for your new and terrible ISP-supplied email. They
were bought by Fastmail, and things mostly still work the way we were used to.
Fastmail seems like a good Australian-based email provider.

Some good Canadian hosts seem to be:

- [CanSpace](https://www.canspace.ca/web-hosting.html)
- [EasyDNS](https://easydns.com/)
- [Varial Hosting](https://varialhosting.com/)
- [Web Hosting Canada](https://whc.ca/canadian-web-hosting)

(CanSpace and Web Hosting Canada appear to Country Flag + in Firefox as being
US-based, I'm hoping that's because they're using Cloudflare or Akami or
something for the website.)

As always, shop carefully to find what you need. I'm going to give Web
Hosting Canada a try when I've got time (my Dreamhost stuff is prepaid for
another two years; I'll definitely move before it expires though).

## Cloud Storage

Cloud storage was actually one of the first things I moved out of American
companies. I was an early adopter of Dropbox, Box, and Google Drive, and my
needs for this are pretty simple. I just want a folder on all my devices that
gets sync'd automatically. It's mostly for my desktop wallpaper collection and
my avatar/meme/random garbage collection.

[Sync.com](https://www.sync.com/) was my favourite; they're Canadian,
everything is encrypted, and their software works well. Unfortunately, when I
switched to Linux, I had to drop them. They don't have a Linux client, and they
don't publish an API so other people could build one. Shame, because that was
basically the same time I became more stringent about paying for all the
services I use and like.

After test driving a bunch of services, I bought a lifetime membership from
[pCloud](https://www.pcloud.com/) who are European and let you choose a Euro
datacentre for your storage. Not as polished as Sync.com, and encryption is
an add-on, but they support all the platforms I use, and it's been solid.

One thing that I haven't addressed yet is my online backups; I've got about
1.2TB of backups in [Backblaze](https://www.backblaze.com/). Backblaze is
a *great* backup solution, although my huge blob is just using their B2 cloud
storage as they don't have a Linux client. I wrote about my backup regime
in a [blog post](/posts/2024/backups/) last year.

This huge hunk of easily accessible online storage costs me less than $10 US
per month, and that includes infinite in/out traffic (which is awesome because
I don't have to worry about running up bills when I test my backup archives).
As far as I can tell (and they do make this difficult to figure out), AWS
would charge me something like $150 US/month for this. I could choose one
of their Canadian datacentres, but it's still an American company.

There are Canadian cloud companies that offer storage, but I've never found
one that competes with Backblaze. Let me know if there's one out there!

Something like [eazyBackup's e3](https://e3.eazybackup.com/) might do the
trick, actually; I think it would cost me about $12 CAD/mo, which is
certainly in the right ballpark.

## Source Code Repos

I moved everything off of GitHub and onto Germany's
[Codeberg](https://codeberg.org/) when GitHub announced they were working on
an LLM. Probably too late, and all my stuff is open source anyway, so it's
entirely symbolic, but if a hugely wealthy company wants to make even more
money from *my* work, I'd really prefer that they pay me. Other developers
good, giant multinationals bad.

Codeberg's also cool because it's built on [Forgejo](https://forgejo.org/) and
[Woodpecker CI](https://woodpecker-ci.org/), both of which are open source
projects.

Codeberg's currently (mid February 2025) being pummelled by
[DDoS and spam attacks](https://blog.codeberg.org/we-stay-strong-against-hate-and-hatred.html)
because fascists hate everything. I also believe that people outside the set
of cishet-white-males are people and can do things, so fuck the fascists.

## Consumer Stuff

I don't really consume a lot of media; I'd usually rather read than watch TV.

These days, I find an e-reader to work best for novels and other things that
don't rely on illustrations or diagrams. Luckily for me, I've always used
[Kobo](https://www.kobo.com/) e-readers, currently a Clara HD. Kobo's a
Canadian company, although they were bought by Japanese giant Rakuten a while
back… Rakuten hasn't made things worse at least from an outsider's point of
view. It's super-easy to load stray epub files from other stores onto the
devices as well, and they work great with [Calibre](https://calibre-ebook.com/).

I don't stream music mostly because I'm an obsessive music collector. I've got
almost 900GB of carefully organized and tagged (well, mostly… it's a work in
progress) music over a huge number of genres, almost 64,000 files. Mostly
ripped from my CD collection or bought as FLACs online. I buy from French
company [Qobuz](https://www.qobuz.com/) or Bandcamp when they do Band Camp
Fridays. While Bandcamp is American, directly supporting the artists I like
is literally the best way to encourage them to keep making awesome music, so
I'm not going to worry about that.

All our streaming is with American companies, and I'm not sure how to address
that other than reading more, or using our DVD/Bluray collection. I usually
prefer movies to shows or series, but my wife is the primary watcher and I'm
not going to try cancelling on her. CBC's [Gem](https://gem.cbc.ca/) has some
great stuff (*Schitt's Creek* has some *fantastic* character development if
you can get past how obnoxious they are in the pilot) and they're my go-to
for when I want to watch some sportsball (Olympics, usually).

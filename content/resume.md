---
title: "Resume"
date: 2024-06-22T09:04:27-04:00
draft: false
showHero: false
tags: ["Portfolio", "Resume"]
categories: ["Work"]
---

- Email: [work-with-chrish@taffer.ca](mailto:work-with-chrish@taffer.ca)
- LinkedIn: [Chris Herborth](https://ca.linkedin.com/in/chrisherborth/)
- Mastodon: [@Taffer@mastodon.gamedev.place](https://mastodon.gamedev.place/@Taffer)

{{< alert "lightbulb" >}}
I'm only interested in full-time, remote positions, thanks. Happy to meet up
with the team when it's for a specific purpose, of course!
{{< /alert >}}

## Summary

- **Leader**: Individual and team development, mentoring, hiring, interviews,
  community building (inclusion), diversity and equity, Agile, Scrum, constant
  improvement, constant learning and relentless curiosity, remote onboarding,
  remote collaboration, tenacious problem-solving, code reviews, Scientific
  Research and Experimental Development (SR&ED) tax credit reporting
- **Outstanding communicator**: 5x winner of the
  [Society for Technical Communications](https://www.stc.org) (STC)
  *Award of Merit*, winner of the STC *Award of Excellence*, Winner of the
  [STC Eastern Ontario Chapter](https://stceo.ca)’s Haiku Contest, Dean's
  Honour List BA in English: Rhetoric and Professional Writing from
  [University of Waterloo](https://uwaterloo.ca); developer to end-user
  translator, user-interface/user-experience (UI/UX) and accessibility advocate
- **Domains**: cryptography, post-quantum cryptography, security, operating systems,
  testing, code quality, threat modelling, vulnerability assessment, privacy,
  developer tools, digital native, hacker ethos
- **Architect**: API design, developer tooling, samples, documentation
- **Full-stack developer**: Python, C, Go, Rust, C#, C++, PHP, JavaScript, CI/CD,
  `git`, MySQL/MariaDB, PostgreSQL, SQLServer, NoSQL, HTML, CSS, Markdown, AWS
  native, FIPS 140, cmake, AsciiDoctor, Doxygen, GitLab, GitHub, Phabricator,
  JIRA, Confluence
- **IT**: infra setup and maintenance, tooling, purchasing, endpoint configuration
- **OSes**: Linux (Ubuntu, Mint, OpenSUSE), macOS, Windows
- **Open source**:
  [AWS-LC](https://github.com/aws/aws-lc),
  [PyGame](https://www.pygame.org/),
  [PNG specification](https://datatracker.ietf.org/doc/html/rfc2083),
  [Info-ZIP](https://infozip.sourceforge.net),
  [Python](https://www.python.org),
  [personal projects](https://codeberg.org/Taffer)
- **Certifications**: Certified Scrum Master (# 142621), CSSLP (# 394125); these
  are expired but could easily be reactivated

## Experience

### Senior Software Engineer (SDE III), Amazon Web Services (Current)

**“Enki”** - Designed and developed an AWS-native code, configuration, and
dependency analyzer for validating FIPS 140-3 implementations across
Amazon-scale code repos and CI pipelines. Development using Python, TypeScript,
AWS SDKs, git, and Amazon CI. Performed design and code reviews. Mentored
software developers, project managers, and program managers.

**AWS-LC** – Added FIPS 140-3 support, including additional cryptographic
algorithms, self-tests, and unit tests for Amazon’s `libcrypto`. Performed code
reviews and design discussions. Mentored software developers, project managers,
and program managers.

**FIPS 140 All The Things** – Shepherding 150+ internal and customer-facing
services towards using exclusively FIPS 140-3 cryptographic libraries for all
data in transit and data at rest. Analyzing system design, code usage, and
threat models to prioritize work required to offer FIPS 140 certified
encryption. Meeting with the service teams to explain the work that needs to be
done, and convince them that the work needs to be done. Managing our side of the
FIPS certification process with our test lab partners. Mentoring software
developers on the FIPS process as well as the work we’re doing at Amazon.

**Diversity, Equity, and Inclusion, Pay Equity, Remote Advocacy** – Actively
trying to improve Amazon’s treatment of employees across the company. Driving
local team-building via lunches and online games. Fostering a stronger team
dynamic and community while working remotely.

### Cryptographic Solutions Manager, ISARA Corporation

**Manager** – Lead the cryptographic research and development team (11 people);
mentored new developers/researchers and co-op students. Developed company coding
standards, portability guidelines, source control and branching standards. Wrote
and presented customer instruction, reports, and coding boot camps. Managed
a company-wide 50% reduction in force.

**Toolkit** – Designed APIs and internal structures, developed building-block
algorithms. Developed samples. Documented APIs, wrote *Developer’s Guide*.
Helped decide which quantum-safe algorithms to implement.

**IT/DevOps/Information Security** – Evaluated and purchased a wide range of
dev/ops team systems (bug tracking, wiki, IM/group chat, continuous integration,
source control, build systems, documentation generation). Documented DevOps
standards and procedures. Designed and documented security policies. Acted as
fire warden and "office dad".

**Inclusiveness** – Investigated inclusivity and how it relates to diversity;
implemented changes to guide the company towards doing better at both. Collected
and documented anecdotes, wrote recommendations.

### Senior Security Automation Developer, BlackBerry/RIM

**CHACE** – Advanced static analysis tooling and prototyping in the Android
kernel for the new Center for High Assurance Computing Excellence.

**“Frontline”** – Designed and developed a cloud service for checking
application reputation at install time on BB10 devices. Investigated and chose
various new technologies (Cassandra, Kafka, OpenNebula). Mentored the team on
new technologies, higher code quality, and Frontline code base, unit tests, and
deployment. *Skills/technologies* - Python, Apache, Cassandra, Kafka, MySQL,
Cloud design, Distributed system design, System architecture, Object-oriented
design, Perforce, Git, Flask microframework, Bootstrap, HTML5, CSS, JIRA, Scrum,
Ubuntu Linux, REST API design, JSON, Web services, Eclipse, Code analysis,
Responsive design, Mobile design.

**“Blacklight”** – Developed new features for an automated malware detection
and analysis engine. Maintained existing large code base while driving for code
quality and unit test coverage. *Skills/technologies* - Python, MySQL, Perforce,
Ubuntu Linux, HTML5, CSS, JIRA, Scrum, Malware analysis, Automated analysis,
Object-oriented design, SOAP, Web services, AJAX, Eclipse, Code analysis.

**“Qpasa”** – Initial design and development of an on-device BlackBerry 10
analysis tool to detect undocumented changes between operating system builds.
*Skills/technologies* - Python, BlackBerry 10, Subversion, Automated analysis,
System architecture.

**OpenGrok** – Set up and maintained a central search repository pulling
together various sources of code from throughout the company.
*Skills/technologies* - OpenGrok, Perforce, Subversion, Git.

**“Dior”** – Designed a Software Readiness Review (SRR) web app that automated
a convoluted business process, making the SRR process repeatable and
significantly faster. *Skills/technologies* - C#, .NET, MVC3, Code analysis,
User-requirements analysis, Scrum, VisualStudio, IIS, MS SQL Server, Subversion.

**“Neo”** – Designed and developed new features for a security project
management web app. *Skills/technologies* - PHP, HTML, CSS, Perforce,
Subversion, Requirements analysis, MySQL.

## Leisure

When I’m not working…

- [Canadian Alternatives - Internet Services](https://codeberg.org/Taffer/canadian-alternatives)
- [Remote Work, 2025 Edition](/posts/2025/remote)
- Reading (fantasy, horror, science-fiction, mythology, ancient history)
- Cycling, walking my dogs
- Video games (I mean, obviously)
- Hobbyist video game development (using Godot these days; PyGame and Löve2D in
  the past)

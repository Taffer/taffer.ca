---
title: "developerWorks Articles"
date: 2024-08-31T08:28:59-04:00
draft: false
showHero: true
tags: ["Howto", "Linux", "Portfolio", "Tools", "Ancient"]
categories: ["Work", "Dev", "Tools", "Linux", "Website"]
---

Articles I wrote ages ago for IBM's defunct developerWorks website.

A long time ago, IBM had a developerWorks website, filled with articles and
tutorials covering a wide range of topics. Then they retired it. The closest
thing they have now is <https://developer.ibm.com/>, which doesn't have any
of the old developerWorks articles as far as I know.

These are all woefully obsolete and *really old*, so they don't reflect my
current writing style, knowledge, etc. They're presented here for archival
purposes only.

I've reformatted them into Markdown, but otherwise left the contents as-is.
Do not trust the contents, as they haven't been updated since these were
originally written in the mid-2000s.

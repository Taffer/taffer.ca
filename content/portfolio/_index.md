---
title: "Portfolio"
date: 2024-02-11T16:24:55-05:00
draft: false
tags: ["Portfolio", "Resume"]
categories: ["Work"]
layout: "single"
---

I've done some things… usually
[things for companies](https://www.linkedin.com/in/chrisherborth/), but the
things here aren't any of those things. I've got my [resume](/resume) (or
CV if you prefer) online here as well.

## Community

To help the community around me:

- [Canadian Alternatives - Internet Services](https://codeberg.org/Taffer/canadian-alternatives) -
  A collection of Canadian alternatives to various Internet and cloud-based
  services.
- [Remote Work, 2025 Edition](/posts/2025/remote) - A big list of companies
  that support remote work, search sites that help to find more, and some
  relevant-to-my-interests sites that list B corporations and green tech jobs.

## Godot Asset Library

In 2024 I started producing useful(?) tidbits for Godot, and posting them to
the [Godot Asset Library](https://godotengine.org/asset-library/) so you can
easily pull them into your projects.

You can see them here: <https://godotengine.org/asset-library/asset?user=Taffer>

Source repos are on [CodeBerg](https://codeberg.org/Taffer), and you can install
them directly from there if you'd prefer.

## Code

I've got some goodies on [CodeBerg](https://codeberg.org/Taffer), ranging from
experiments using different game engines (Löve2D, PyGame, and Godot mostly),
useful(?) scripts for things like automated backups, out of date HTML-based
resumes, etc. I use CodeBerg for all of my open source stuff (usually MIT
licensed or Creative Commons) because they're a non-profit and they're not
using all the public repos to train an LLM.

There are a few things on [GitHub](https://github.com/Taffer), although I only
use it for forking projects that are already on GitHub. If Microsoft wants me
to help train their code LLM(s), they can pay me for the privilege.

Some open source things I've worked on in the past:

* [AWS-LC](https://github.com/aws/aws-lc) - This was actually part of my day
  job; I added some KDF code, and some tests and tweaks to support the
  FIPS 140-3 certification.
* [PyGame](https://www.pygame.org/news) - Contributed some doc improvements.
* [Python](https://www.python.org/) - I used to maintain ports of Python to
  a couple of rare operating systems (BeOS and QNX), and wrote some modules for
  BeOS so you could manipulate the extended attributes found in the filesystem
  and for [application scripting](https://codeberg.org/Taffer/BeOS). I lost
  the filesystem module source code at some point in a tragic disk crash.
* [Info-ZIP](https://sourceforge.net/projects/infozip/) - I maintained the QNX
  and BeOS ports back in the day.

## Writing

I started my career as a technical writer, a *very technical* technical writer.
For years I worked documenting APIs and OSes, and sometimes working on books.

Technical stuff:

* [PNG image specification](https://datatracker.ietf.org/doc/html/rfc2083) -
  I mostly did tech editing for this.
* *Unix Advanced: Visual QuickPro Guide* for
  [PeachPit](https://www.peachpit.com/) - This was probably obsolete as soon
  as it was published, and it was never popular. At the time, ridiculous 1000
  page learn-it-all books were the rage, so a small focused volume wasn't
  going to cut it.
* [*The BeOS Bible*](https://archive.org/details/the-beos-bible) with Scot
  Hacker and Henry Bortman - Technical editing, but also a huge chapter on
  application scripting (delivered online). I found an ancient PDF of the
  scripting chapter if you want to [read it](BeOS.scripting.pdf).
* A bunch of articles for IBM's dead developerWorks site, about a wide range
  of things (making PDFs programmatically from PHP, XML, UNIX). I've started
  putting them online [here](developerworks/) for your amusement.
* Technical editor for a bunch of
  [Martin Brown's books](https://www.goodreads.com/author/list/49437.Martin_C_Brown).

Not technical:

* Some fiction here and there, mostly terrible so it's not online. `;-)` I've
  got a couple things on [WattPad](https://www.wattpad.com/user/punksdad) if
  you're curious.

## Copious Spare Time

I spend a lot of my "free" time studying video games, learning things related
to video games, thinking about making video games, and sometimes even making
video games.  This is literally something I've wanted to do since I first ran
into computers.

Somehow the only "complete" (that is to say, playable) game I've done is a
[Robots](https://codeberg.org/Taffer/robots-game) game that I made in
[Godot](https://godotengine.org/) based on a prompt from
[@gamedevchallengewithgodot](https://mastodon.gamedev.place/@gamedevchallengewithgodot/109496361250432201)
on Mastodon.

Current projects, moving forward at a snail's pace, include:

* [*Arcana*](/games/arcana) - Originally conceived as an *Ultima 2* tribute,
  currently some sort of *Ultima 4*/*Ultima 5*/*Legend of Zelda*/JRPG mashup
  type of thing.
* [*Schrödinger's Chicken*](/games/schrodingers-chicken) - A super basic
  click-the-thing game so I can learn more about Godot with something really
  small.
* [*Skelly*](/games/skelly) - A top-down action RPG type thing; play as a
  skeleton in a story of the
  [Skeleton War](https://knowyourmeme.com/memes/events/skeleton-war).
* [*Untitled Ancient Egyptian CRPG*](/games/book-of-the-dead) - You've been
  murdered, and need to solve your own murder from the afterlife.
